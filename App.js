import React from 'react'
import {Root} from  'native-base';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import {ApolloProvider} from 'react-apollo';
import AWSAppSyncClient from 'aws-appsync';
import configVariables from './config/AppSync'
import StartPage from './Component/StartPage'
import Login from './Component/Login'
import SendOtp from './Component/SendOtp'
import EnterOtp from './Component/EnterOtp'
import HomeScreen from './Component/HomeScreen'
import BookAppointment from './Component/BookAppointment'
import CaptureVitals from './Component/CaptureVitals'
import UploadReport from './Component/UploadReport'
import CaptureVManually from './Component/CaptureVManually'
import CaptureVDevice from './Component/CaptureVDevice'
import PairDevice from './Component/PairDevice'
import PairingDevice from './Component/PairingDevice'
import PairingDeviceS from './Component/PairingDeviceS'
import StartTest from './Component/StartTest'
import TestProcessing from './Component/TestProcessing'
import TestCompleted from './Component/TestCompleted'
import DataCaptured from './Component/DataCaptured'
import MyDoctor from './Component/MyDoctor'
import MyReport from './Component/MyReport'
import MyInsurance from './Component/MyInsurance'
import ViewInsurance from './Component/ViewInsurance'
import AddInsurance from './Component/AddInsurance'
import MedicalServices from './Component/MedicalServices'
import Notifications from './Component/Notification'
import MyDoctorS from './Component/MyDoctorS'
import MyAccount from './Component/MyAccount'
import EmergencyContact from './Component/EmergencyContact'
import MyFamily from './Component/MyFamily'
import MyReports from './Component/MyReportS'
import MyAppointments from './Component/MyAppointments'
import SpecificSpeciality from './Component/SpecificSpeciality'
import SearchSpeciality from './Component/SearchSpeciality'
import MyVitals from './Component/MyVitals'
import CliniQWallet from './Component/Wallet/CliniQWallet'
import AuthorisePayment from './Component/Wallet/AuthorisePayment'
import Addmoney from './Component/Wallet/Addmoney'
import Integrationpage from './Component/Wallet/Integrationpage'
import Receipt from './Component/Wallet/Receipt'
import AuthOTP from './Component/Wallet/AuthOTP'
import QRcode from './Component/Wallet/QRCode'
import Confirmation from './Component/Confirmation'
import BookApp from './Component/BookApp'
import Doctorprofile from './Component/Doctorprofile'
import MyProfile from './Component/MyProfile'
import MainTab from './Component/MainTab'
import Tutorials from './Component/Tutorials'
import Referafriend from './Component/Referafriend'
const client = new AWSAppSyncClient({
  url: configVariables.graphqlEndpoint,
  region: configVariables.region,
  auth: {
    type: configVariables.authenticationType,
    apiKey: configVariables.apiKey,
  },
  disableOffline: true,
  complexObjectsCredentials: () => {
    return new AWS.Credentials({
      accessKeyId: configVariables.complexObjectsCredentialsAccessKeyId,
      secretAccessKey: configVariables.complexObjectsCredentialsSecretAccessKey,
    });
  },
});

const AppNavigator = createStackNavigator(
  {

    StartPage: {
      screen: StartPage, navigationOptions: {
        header: null,
      },
    },
    Login: {
      screen: Login, navigationOptions: {
        header: null,
      },
    },
    SendOtp: {
      screen: SendOtp, navigationOptions: {
        header: null,
      },
    },
    EnterOtp: {
      screen: EnterOtp, navigationOptions: {
        header: null,
      },
    },
    HomeScreen: {
      screen: HomeScreen, navigationOptions: {
        header: null,
      },
    },
    BookAppointment: {
      screen: BookAppointment, navigationOptions: {
        header: null,
      },
    },
    CaptureVitals: {
      screen: CaptureVitals, navigationOptions: {
        header: null,
      },
    },
    UploadReport: {
      screen: UploadReport, navigationOptions: {
        header: null,
      },
    },
    CaptureVManually: {
      screen: CaptureVManually, navigationOptions: {
        header: null,
      },
    },
    CaptureVDevice: {
      screen: CaptureVDevice, navigationOptions: {
        header: null,
      },
    },
    PairDevice: {
      screen: PairDevice, navigationOptions: {
        header: null,
      },
    },
    PairingDevice: {
      screen: PairingDevice, navigationOptions: {
        header: null,
      },
    },
    PairingDeviceS: {
      screen: PairingDeviceS, navigationOptions: {
        header: null,
      },
    },
    StartTest: {
      screen: StartTest, navigationOptions: {
        header: null,
      },
    },
    TestProcessing: {
      screen: TestProcessing, navigationOptions: {
        header: null,
      },
    },
    TestCompleted: {
      screen: TestCompleted, navigationOptions: {
        header: null,
      },
    },
    DataCaptured: {
      screen: DataCaptured, navigationOptions: {
        header: null,
      },
    },
    MyDoctor: {
      screen: MyDoctor, navigationOptions: {
        header: null,
      },
    },
    MyReport: {
      screen: MyReport, navigationOptions: {
        header: null,
      },
    },
    MyInsurance: {
      screen: MyInsurance, navigationOptions: {
        header: null,
      },
    },
    ViewInsurance: {
      screen: ViewInsurance, navigationOptions: {
        header: null,
      },
    },
    AddInsurance: {
      screen: AddInsurance, navigationOptions: {
        header: null,
      },
    },
    MedicalServices: {
      screen: MedicalServices, navigationOptions: {
        header: null,
      },
    },
    Notifications: {
      screen: Notifications, navigationOptions: {
        header: null,
      },
    },
    MyDoctorS: {
      screen: MyDoctorS, navigationOptions: {
        header: null,
      },
    },
    MyAccount: {
      screen: MyAccount, navigationOptions: {
        header: null,
      },
    },
    EmergencyContact: {
      screen: EmergencyContact, navigationOptions: {
        header: null,
      },
    },
    MyFamily: {
      screen: MyFamily, navigationOptions: {
        header: null,
      },
    },
    MyReports: {
      screen: MyReports, navigationOptions: {
        header: null,
      },
    },
    MyAppointments: {
      screen: MyAppointments, navigationOptions: {
        header: null,
      },
    },
    SpecificSpeciality: {
      screen: SpecificSpeciality, navigationOptions: {
        header: null,
      },
    },
    SearchSpeciality: {
      screen: SearchSpeciality, navigationOptions: {
        header: null,
      },
    },
    CliniQWallet: {
      screen: CliniQWallet, navigationOptions: {
        header: null,
      },
    },
    AuthorisePayment: {
      screen: AuthorisePayment, navigationOptions: {
        header: null,
      },
    },
    Addmoney: {
      screen: Addmoney, navigationOptions: {
        header: null,
      },
    },
    Integrationpage: {
      screen: Integrationpage, navigationOptions: {
        header: null,
      },
    },
    Receipt: {
      screen: Receipt, navigationOptions: {
        header: null,
      },
    },
    AuthOTP: {
      screen: AuthOTP, navigationOptions: {
        header: null,
      },
    },
    QRcode: {
      screen: QRcode, navigationOptions: {
        header: null,
      },
    },
    MyVitals: {
      screen: MyVitals, navigationOptions: {
        header: null,
      },
    },
    Confirmation: {
      screen: Confirmation, navigationOptions: {
        header: null,
      },
    },
    BookApp: {
      screen: BookApp, navigationOptions: {
        header: null,
      },
    },
    Doctorprofile: {
      screen: Doctorprofile, navigationOptions: {
        header: null,
      },
    },
    MyProfile: {
      screen: MyProfile, navigationOptions: {
        header: null,
      },
    },
    MainTab: {
      screen: MainTab, navigationOptions: {
        header: null,
      },
    },
    Tutorials: {
      screen: Tutorials, navigationOptions: {
        header: null,
      },
    },
    Referafriend: {
      screen: Referafriend, navigationOptions: {
        header: null,
      },
    },
   
  },

  {
    initialRouteName: 'StartPage',
    headerMode: 'none',
    mode: 'modal'
  }
);

const Name = (props) => (
  <ApolloProvider client={client}>
    <Root>
    <AppNavigator />
    </Root>
  </ApolloProvider>
);
export default Name;