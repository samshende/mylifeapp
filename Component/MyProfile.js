import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    TouchableWithoutFeedback
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign'
import Icon1 from 'react-native-vector-icons/FontAwesome'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';



export default class MyProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action: false
        };
    }


    render() {
        return (
            <Container>
                <View style={{ backgroundColor: '#707373', width: wp('100%'), height: 70 }}>
                    <View style={styles.headerView}>
                        <View style={{ justifyContent: 'flex-start', flexDirection: 'row', alignSelf: 'center' }}>
                            <Icon onPress={() => { this.props.navigation.navigate('MyAccount') }} name='arrowleft' size={25} color='white' />
                            <Text style={styles.text}>My Profile</Text>
                        </View>
                        <Thumbnail style={styles.img} large source={require('../Public/img/clinic-logo-png.png')} />
                    </View>
                </View>
                <Tabs style={{ width: wp('90%'), alignSelf: 'center', marginTop: -10 }} tabBarUnderlineStyle={{ borderBottomWidth: 3, borderBottomColor: 'grey' }}>
                    <Tab heading="Personal Information"
                        tabStyle={{ backgroundColor: 'white' }}
                        activeTabStyle={{ backgroundColor: 'white' }}
                        textStyle={{ textAlign: 'center', color: 'grey' }}
                        activeTextStyle={{ color: 'black', textAlign: 'center' }} >
                        <Card style={styles.card}>
                            <View style={styles.view}>
                                        <Text style={{color: 'grey' }}>Personal Information</Text>
                            </View>
                        </Card>
                    </Tab>
                    <Tab heading="Medical"
                        tabStyle={{ backgroundColor: 'white' }}
                        activeTabStyle={{ backgroundColor: 'white' }}
                        textStyle={{ textAlign: 'center', color: 'grey' }}
                        activeTextStyle={{ color: 'black', textAlign: 'center' }}>
                        <Card style={styles.card}>
                            <View style={styles.view}>
                            <Text style={{color: 'grey' }}>Medical Information like</Text>
                            <Text style={{color: 'grey' }}>allergies</Text>
                            <Text style={{color: 'grey' }}>medications</Text>
                            <Text style={{color: 'grey' }}>Injurles</Text>

                            </View>

                        </Card>
                    </Tab>
                    <Tab heading="Lifestyle"
                        tabStyle={{ backgroundColor: 'white' }}
                        activeTabStyle={{ backgroundColor: 'white' }}
                        textStyle={{ textAlign: 'center', color: 'grey' }}
                        activeTextStyle={{ color: 'black', textAlign: 'center' }}>
                        <Card style={styles.card}>
                            <View style={styles.view}>
                            <Text style={{color: 'grey' }}>Medical Information like</Text>
                            <Text style={{color: 'grey' }}>Food Habits</Text>
                            <Text style={{color: 'grey' }}>Alcohol</Text>
                            <Text style={{color: 'grey' }}>Smoking</Text>
                            </View>
                        </Card>
                    </Tab>
                </Tabs>

            </Container>
        );
    }
}

const styles = StyleSheet.create({

    headerView: {
        justifyContent: 'space-between', flexDirection: 'row', width: wp('93%'), alignSelf: 'center', marginTop: 10
    },
    img: {
        height: 40, width: 40, borderRadius: 20, alignSelf: 'center', backgroundColor: 'white'
    },
    text: {
        marginLeft: 20, color: 'white', fontWeight: 'bold', fontSize: 18
    },
    card: {
        height: 450, marginTop: 20, borderWidth: 4, borderColor: 'grey'
    },
    view:{
        justifyContent:'center',flex:1,alignItems:'center',alignContent:'center'
    }
});
