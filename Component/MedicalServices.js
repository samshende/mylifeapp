import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather'
import Icon1 from 'react-native-vector-icons/FontAwesome'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';


export default class MedicalServices extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action: false
        };
    }
    // ViewInsurance
    // AddInsurance

    render() {
        console.log("props",this.props)
        return (
            <Container>
                <Header style={styles.header}>
                    <Left>
                        <Icon name='menu' size={30} />
                    </Left>
                    <Body>
                        <Text style={{ textAlign: 'center' }}>Medical Services</Text>
                    </Body>
                    <Right>
                        <Icon1 onPress={() => { this.props.navigation.navigate('Notifications')}}  style={styles.icon} color='#7a7979' name='bell' size={30} />
                        <Icon1 style={styles.icon} color='#7a7979' name='user-circle' size={30} />
                    </Right>
                </Header>
                <ScrollView>
                    <Card style={styles.card}>
                        <View style={styles.viewCard}>
                            <Text style={{marginLeft:20,fontSize:22}}>Location Filtered</Text>
                            <Text style={{marginLeft:20,fontSize:22}}>list of Services</Text>

                        </View>

                    </Card>
                </ScrollView>
                </Container>
        );
    }
}

const styles = StyleSheet.create({

    header: {
        backgroundColor: 'white'
    },

    icon: {
        marginLeft: 15
    },
  card:{
      alignSelf:'center',width:wp('90%'),height:150,marginTop:40,borderColor:'black'
  },
  viewCard:{
      justifyContent:'center',flex:1
  }

});
