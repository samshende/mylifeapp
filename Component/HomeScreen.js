import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    Modal,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather'
import Icon1 from 'react-native-vector-icons/FontAwesome'
import DrawerComp from './Drawer'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';

const ActionArray = [
    {
        name: 'Book Appointment',
        route: 'BookAppointment',
        key: 0
    },
    {
        name: 'Capture Vitals',
        route: 'CaptureVitals',
        key: 1
    },
    {
        name: 'Upload a Report',
        route: 'UploadReport',
        key: 2
    }
]

export default class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action: false,
            Drawer: false
        };
    }

ModalClose=()=>{
    this.setState({
        Drawer:false
    })
}

    render() {
        console.log("props",this.props)
        return (
            <Container>
                <Header style={styles.header}>
                    <Left>
                        <Icon onPress={() => { this.setState({Drawer:true})}} name='menu' size={30} />
                    </Left>
                    <Right>
                        <Icon1 onPress={() => { this.props.navigation.navigate('Notifications') }} style={styles.icon} color='#7a7979' name='bell' size={30} />
                        <Icon1  style={styles.icon} color='#7a7979' name='user-circle' size={30} />
                    </Right>
                </Header>
                <View style={styles.mainView}>
                    <Text style={styles.text} onPress={() => { this.props.navigation.navigate('MainTab') }}>Menu</Text>
                    <Text style={styles.texth} >Home Screen </Text>
                    <Text style={styles.texth} >Content tbd </Text>
                    {this.state.action &&
                        <Card style={styles.bottomCard}>
                            <View style={styles.cardView}>
                                {ActionArray.map((i, j) => {
                                    return (
                                        <Text onPress={() => {this.setState({action:false}) 
                                        this.props.navigation.navigate(`${i.route}`) }} style={styles.cardText}>{i.name}</Text>
                                    )
                                })}
                            </View>
                        </Card>
                    }

                    <View style={styles.bottomView}>
                        <Text style={{ alignSelf: 'center', marginRight: 10 }}>Quick Action</Text>
                        {this.state.action ?
                            <Icon1 onPress={() => { this.setState({ action: !this.state.action }) }} style={{}} color='#7a7979' name='times-circle' size={70} />
                            :
                            <Icon1 onPress={() => { this.setState({ action: !this.state.action }) }} style={{}} color='#7a7979' name='plus-circle' size={70} />
                        }
                    </View>

                </View>
                <Modal
                    // animationType="slide"
                    transparent={true}
                    visible={this.state.Drawer}
                    onRequestClose={() => {
                        // Alert.alert("Modal has been closed.");
                        this.setState({Drawer:false})
                    }}
                >
              <DrawerComp props1={this.props.navigation.navigate}  closeModal={this.ModalClose}/>
                </Modal>
            </Container>
        );
    }
}

const styles = StyleSheet.create({

    mainView: {
        flex: 1,
        alignItems: "center",
        alignContent: 'center',
        alignSelf: 'center',
        width: wp('95%'),
        justifyContent:'center'
    },
    header: {
        backgroundColor: 'white'
    },
    text: {
        position:'absolute',
        top:10,left:10
    },
    icon: {
        marginLeft: 15
    },
    texth: {
        fontSize: 20,
        textAlign: 'center',
        fontWeight: 'bold'
    },
    bottomView: {
        alignSelf: 'flex-end',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        position: 'absolute',
        bottom: 20,
        right: 10
    },
    bottomCard: {
        alignSelf: 'flex-end',
        position: 'absolute',
        bottom: 90,
        right: 10,
        height: 'auto',
        width: wp('60%'),
    },
    cardView: {
        margin: 20,
        marginLeft: 40,
        marginRight: 10
    },
    cardText: {
        marginTop: 5, marginBottom: 5, fontWeight: 'bold', color: '#7a7979', fontSize: 17
    }
});
