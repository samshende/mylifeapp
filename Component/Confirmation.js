
import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather'
import Icon1 from 'react-native-vector-icons/FontAwesome'
import Icon2 from 'react-native-vector-icons/AntDesign'
import Icon3 from 'react-native-vector-icons/AntDesign'


import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';

export default class Confirmation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action: false
        };
    }


    render() {
        return (
            <Container style={{}}>
                <View style={{ backgroundColor: '#2a85c7', width: wp('100%'), height: 70 }}>
                    <View style={styles.headerView}>
                        <View style={{ justifyContent: 'flex-start', flexDirection: 'row', alignSelf: 'center' }}>
                            <Icon3 onPress={() => { this.props.navigation.navigate('BookApp') }} name='arrowleft' size={25} color='white' />
                            <Text style={styles.text}>Book An Appointment</Text>
                        </View>
                        <Thumbnail style={styles.img1} large source={require('../Public/img/clinic-logo-png.png')} />
                    </View>
                </View>
                <ScrollView>
                    <View style={{ height: 160, backgroundColor: '#59c9c2', borderTopWidth: 1, borderTopColor: 'white' }}>
                        <Text style={{ alignSelf: 'center', marginTop: 5, color: 'white' }}>Book Your Appointment with</Text>
                        <View style={styles.viewTop1}>
                            <View style={{ width: wp('19%') }}>
                                <Image
                                    resizeMode='contain'
                                    style={styles.img}
                                    source={require('../Public/img/doc3.jpeg')}
                                />
                            </View>
                            <Card style={styles.card}>
                                <View style={{ position: 'absolute', right: 0, bottom: 0, backgroundColor: '#19cbe3', height: 30, width: 30, }}>
                                    <Icon2 style={{ alignSelf: 'center', marginTop: 4 }} name='infocirlceo' size={20} color='white' />
                                </View>
                                <View style={{ margin: 10 }}>
                                    <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#5e5d5d' }}>Dr. Uday Shetty</Text>
                                    <Text style={{ fontSize: 11, color: 'grey' }}>General Medicine</Text>
                                    <Text style={{ fontWeight: 'bold', fontSize: 13, color: '#5e5d5d' }}>Qualification</Text>
                                    <Text style={{ fontSize: 12, color: 'grey' }}>MBBS,MS</Text>
                                </View>
                            </Card>
                        </View>
                    </View>
                    <View style={{ height: 330, backgroundColor: '#edebeb', }}>
                        <View style={{ margin: 10 }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 15 }}>Appointment Location</Text>
                            <Text style={{ fontSize: 13, color: 'grey' }}>Tulsi Green, Laxman Nagar, Baner, Pune, Maharashtra 411045</Text>
                            <Text style={{ fontWeight: 'bold', fontSize: 15 }}>Appointment Details</Text>
                            <Text style={{ fontSize: 13, color: 'grey' }}>New Appointment on 19th May,10.00am</Text>
                        </View>
                        <View style={styles.viewTop2}>
                            <View style={{ width: wp('47%') }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 15 }}>Visit Fee:</Text>
                                <Text style={{ fontWeight: 'bold', fontSize: 15 }}>Rs.1500
                        <Text style={{ fontSize: 13, color: 'grey' }}> for a
                        </Text > 30 min
                        <Text style={{ fontSize: 13, color: 'grey' }}> session
                        </Text></Text>
                            </View>
                            <View style={{ width: wp('47%') }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 15 }}>Consulting for:</Text>
                                <Text style={{ fontWeight: 'bold', fontSize: 15 }}>Gastrology</Text>
                            </View>
                        </View>

                        <View style={styles.viewTop3}>
                            <View style={{ width: wp('47%') }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 15 }}>Patient Details</Text>
                                <Text style={{ fontSize: 13, color: 'grey' }}>Mr.Nitin Patil</Text>
                                <Text style={{ fontSize: 13, color: 'grey' }}>+919909090786</Text>
                                <Text style={{ fontSize: 13, color: 'grey' }}>shivam@gmail.com</Text>
                            </View>
                            <View style={{ width: wp('47%') }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 15 }}>Purpose of Appointment</Text>
                                <Text style={{ fontSize: 13, color: 'grey' }}>health</Text>
                            </View>
                        </View>
                        <View style={{ justifyContent: 'flex-start', flexDirection: 'row', alignSelf: 'center', width: wp('80%'), marginTop: 10 }}>
                            <Icon style={{ alignSelf: 'center' }} color='#e8c917' name='check-circle' size={25} />
                            <Text style={{ fontSize: 20, fontWeight: 'bold', marginLeft: 10, textAlign: 'center',color:'#e8c917' }}>Appointment Confirmed</Text>
                        </View>
                        <Text style={{ textAlign: 'center', fontSize: 13, color: 'grey',marginTop:10 }}>An email regarding the same has been sent to you</Text>
                    </View>
                    <Button full style={styles.buttonReg} onPress={() => { this.props.navigation.navigate('HomeScreen') }}><Text>To Home</Text></Button>
                </ScrollView>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: 'white',
    },
    textHeader: {
        alignSelf: 'center',
        fontSize: 18,
        fontWeight: 'bold'
    },
    viewTop: {
        alignSelf: 'center',
        width: wp('92%'),
        justifyContent: 'space-between',
        flexDirection: 'row', marginTop: 10, marginBottom: 10
    },
    viewTop1: {
        alignSelf: 'center',
        width: wp('90%'),
        justifyContent: 'flex-start',
        flexDirection: 'row',
        marginTop: 10
    },
    card: {
        width: wp('70%'),
        height: 100,
        backgroundColor: '#d0f7f1'
        // borderRadius: 6
    },
    img: {
        height: 90, width: wp('20%'), marginTop: 7
    },
    headerView: {
        justifyContent: 'space-between', flexDirection: 'row', width: wp('93%'), alignSelf: 'center', marginTop: 10
    },
    img1: {
        height: 40, width: 40, borderRadius: 20, alignSelf: 'center', backgroundColor: 'white'
    },
    text: {
        marginLeft: 20, color: 'white', fontSize: 18
    },
    viewTop2: {
        alignSelf: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    viewTop3: {
        alignSelf: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginTop:10
    },
    buttonReg: {
        width: wp('80%'),
        alignSelf: 'center',
        backgroundColor:'#2a85c7',
        marginBottom:15,
        marginTop:10
    },

});
