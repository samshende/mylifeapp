import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather'
import Icon1 from 'react-native-vector-icons/FontAwesome'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';

const ActionArray = [
    {
        name: 'Document Name',
        tag: 'Document Tag',
        doctor: 'Doctor Name',
        day: 'Tue',
        date: '24 May',
        time: '10.30 Am',
        key: 0
    },
    {
        name: 'Document Name',
        tag: 'Document Tag',
        doctor: 'Doctor Name',
        day: 'Tue',
        date: '24 May',
        time: '10.30 Am',
        key: 1
    },
    {
        name: 'Document Name',
        tag: 'Document Tag',
        doctor: 'Doctor Name',
        day: 'Tue',
        date: '24 May',
        time: '10.30 Am',
        key: 3
    },
]

export default class MyInsurance extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action: false
        };
    }
    // ViewInsurance
    // AddInsurance

    render() {
        return (
            <Container>
                <Header style={styles.header}>
                    <Left>
                        <Icon name='menu' size={30} />
                    </Left>
                    <Body>
                        <Text style={{ textAlign: 'center' }}>My Insurance</Text>
                    </Body>
                    <Right>
                        <Icon1 onPress={() => { this.props.navigation.navigate('Notifications') }} style={styles.icon} color='#7a7979' name='bell' size={30} />
                        <Icon1 style={styles.icon} color='#7a7979' name='user-circle' size={30} />
                    </Right>
                </Header>
                <ScrollView>
                    <Text style={styles.text} onPress={() => { this.props.navigation.navigate('AddInsurance') }}>Add New</Text>
                    {ActionArray.map((i, j) => {
                        return (
                            <View style={styles.mainView}>
                                <Card style={styles.card1}>
                                    <View style={styles.card1view}>
                                        <Text style={{ color: 'white', fontSize: 12 }}>{i.day}</Text>
                                        <Text style={{ color: 'white', fontSize: 15, fontWeight: 'bold' }}>{i.date}</Text>
                                        <Text style={{ color: 'white', fontSize: 12 }}>{i.time}</Text>
                                    </View>
                                </Card>
                                <Card style={styles.card2}>
                                    <View style={styles.card2view}>
                                        <View style={{}}>
                                            <Text style={{ fontSize: 15, color: '#707073' }}>{i.name}</Text>
                                            <Text style={{ fontSize: 10, color: 'grey' }}>{i.tag}</Text>
                                            <Text style={{ fontSize: 13, fontWeight: 'bold' }}>{i.doctor}</Text>
                                        </View>
                                        <Text style={{ alignSelf: 'center', color: '#67547d' }} onPress={() => { this.props.navigation.navigate('ViewInsurance') }}>View</Text>
                                    </View>
                                </Card>

                            </View>
                        )

                    })}
                </ScrollView>
            </Container>
        );
    }
}

const styles = StyleSheet.create({

    header: {
        backgroundColor: 'white'
    },

    icon: {
        marginLeft: 15
    },
    text: {
        color: '#67547d',
        marginTop: 20, marginLeft: 20, marginBottom: 20
    },
    mainView: {
        justifyContent: 'flex-start', flexDirection: 'row', width: wp('95%'), alignSelf: 'center',marginBottom:10
    },
    card1: {
        width: wp('28%'),
        height: 90,
        backgroundColor: '#3d68ad'
    },
    card2: {
        width: wp('65%'),
        height: 90, marginLeft: -2
    },
    card1view: {
        margin: 10, marginLeft: 15
    },
    card2view: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        margin: 10
    }

});
