

import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign'
import Icon1 from 'react-native-vector-icons/FontAwesome'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';



export default class ViewInsurance extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action:false
        };
    }


    render() {
        return (
            <Container style={{backgroundColor:'white'}}>
                    <View style={{backgroundColor:'#707373',width:wp('100%'),height:70}}>
                    <View style={styles.headerView}>
                            <View style={{justifyContent:'flex-start',flexDirection:'row',alignSelf:'center'}}>
                            <Icon onPress={() => { this.props.navigation.navigate('MyInsurance') }} name='arrowleft' size={25} color='white' />
                            <Text style={styles.text}>My Insurance</Text>
                        </View>
                        <Thumbnail style={styles.img} large  source={require('../Public/img/clinic-logo-png.png')} />
                    </View>
                    </View>
                    <Header style={styles.header}>
                        <Body>
                            <Text style={{color:'white'}}>Document Name</Text>
                            </Body>
                            <Right>
                           
                            <Text style={styles.textheader}>Share</Text>
                            <Text style={styles.textheader}>Delete</Text>
                            </Right>

                    </Header>
                    <ScrollView>
                        <Card style={styles.card}>

                        </Card>
                        
                        
                    </ScrollView>
              </Container>
        );
    }
}

const styles = StyleSheet.create({
    headerView:{
        justifyContent:'space-between',flexDirection:'row',width:wp('93%'),alignSelf:'center',marginTop:10
    },
    img:{
        height:40,width:40,borderRadius:20,alignSelf:'center',backgroundColor:'white'
    },
    text:{
        marginLeft:20,color:'white',fontWeight:'bold',fontSize:18
    },
    header:{
        backgroundColor:'#a8a7a3'
    },
    card:{
        height:hp('70%'),
        width:wp('92%'),
        alignSelf:'center',
        marginTop:20,
        borderColor:'black'
    },
    textheader:{
        marginRight:12,color:'white',fontSize:12
    }
   
});
