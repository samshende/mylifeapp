
import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    ScrollView,
    TouchableWithoutFeedback
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather'
import Icon1 from 'react-native-vector-icons/FontAwesome'
import Icon2 from 'react-native-vector-icons/AntDesign'


import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';
import RNPickerSelect from 'react-native-picker-select';
const speciality = [
    { value: "Gastrologist", label: "Gastrologist" },
    { value: "General Physical", label: "General Physical" },
    { value: "speciality 3", label: "speciality 3" },
    { value: "speciality 4", label: "speciality 4" },
];

const Location = [
    { value: "Hinjawadi", label: "Hinjawadi" },
    { value: "Baner", label: "Baner" },
    { value: "pune", label: "pune" },
];

export default class BookApp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action: false,
            Check1: [
                { name: 'New Appointment', check: true, id: 0 },
                { name: 'Follow Up', check: false, id: 1 },
                { name: 'Teleconsult', check: false, id: 2 }
            ],
            arr:[
                {
                    id:0,
                    time:'9.00am',
                    status:false
                },
                {
                    id:1,
                    time:'10.00am',
                    status:false
                }, 
                 {
                    id:2,
                    time:'10.30am',
                    status:false
                },
                  {
                    id:3,
                    time:'11.00am',
                    status:false
                },
                {
                    id:4,
                    time:'12.00am',
                    status:false
                },
                {
                    id:5,
                    time:'6.00pm',
                    status:false
                },
                {
                    id:6,
                    time:'6.30pm',
                    status:false
                }, 
                 {
                    id:7,
                    time:'7.00pm',
                    status:false
                },
                  {
                    id:8,
                    time:'8.00pm',
                    status:false
                },
                {
                    id:9,
                    time:'9.00pm',
                    status:false
                },
            ],
            weekArray:[
                {
                    day:'Mon',
                    date:'17',
                    id:1,
                    select:false
                },
                {
                    day:'Tue',
                    date:'18',
                    id:2,
                    select:false
                },
                {
                    day:'Wed',
                    date:'19',
                    id:3,
                    select:false
                },
                {
                    day:'Thu',
                    date:'20',
                    id:4,
                    select:false
                },
                {
                    day:'Fri',
                    date:'21',
                    id:5,
                    select:false
                },
                {
                    day:'Sat',
                    date:'22',
                    id:6,
                    select:false
                },
                {
                    day:'Sun',
                    date:'23',
                    id:7,
                    select:false
                },
            ]

        };
    }

    selectSlote=(i)=>{
        let changeStatus = ''
                    changeStatus = this.state.arr.find((cb) => cb.id === i.id);
                    changeStatus.status =!changeStatus.status
                    console.log("c", changeStatus)
                    const arr = Object.assign([], this.state.arr, changeStatus);
                    this.setState({
                       arr:arr
                    })
    }

    selectTaimeAndDate=(i)=>{
        let selectTime = ''
                    selectTime = this.state.weekArray.find((cb) => cb.id === i.id);
                    selectTime.select =!selectTime.select
                    console.log("c", selectTime)
                    const arr = Object.assign([], this.state.weekArray, selectTime);
                    this.setState({
                       weekArray:arr
                    })
    }

    onChangeSpeci = (value) => {
        console.log("val", value)
    }
    onChangeLocation = (value) => {
        console.log("val", value)
    }

    checkChange = (i) => {
        let checkbox = ''
        checkbox = this.state.Check1.find((cb) => cb.id === i.id);
        checkbox.check = !checkbox.check
        console.log("c", checkbox)
        const Check1 = Object.assign([], this.state.Check1, checkbox);
        this.setState({
            Check1: Check1
        })
    }


    render() {
        const placeholder = {
            label: 'Select speciality you are seeking consultation for',
            value: null,
            color: '#8d8f91',
        };
        const placeholder1 = {
            label: 'Select Appointment Location',
            value: null,
            color: '#8d8f91',
        };
        return (
            <Container style={{}}>
                <View style={{ backgroundColor: '#2a85c7', width: wp('100%'), height: 70 }}>
                    <View style={styles.headerView}>
                        <View style={{ justifyContent: 'flex-start', flexDirection: 'row', alignSelf: 'center' }}>
                            <Icon2 onPress={() => { this.props.navigation.navigate('BookAppointment') }} name='arrowleft' size={25} color='white' />
                            <Text style={styles.text}>Book An Appointment</Text>
                        </View>
                        <Thumbnail style={styles.img1} large source={require('../Public/img/clinic-logo-png.png')} />
                    </View>
                </View>
                <ScrollView>
                    <View style={{ height: 160, backgroundColor: '#59c9c2', }}>
                        <Text style={{ alignSelf: 'center', marginTop: 5, color: 'white' }}>Book Your Appointment with</Text>
                        <View style={styles.viewTop1}>
                            <View style={{ width: wp('19%') }}>
                                <Image
                                    resizeMode='contain'
                                    style={styles.img}
                                    source={require('../Public/img/doc3.jpeg')}
                                />
                            </View>
                            <Card style={styles.card}>
                            <TouchableWithoutFeedback onPress={() => { this.props.navigation.navigate('Doctorprofile') }}>
                                <View style={{ position: 'absolute', right: 0, bottom: 0, backgroundColor: '#19cbe3', height: 30, width: 30, }}>
                                    <Icon2 style={{ alignSelf: 'center', marginTop: 4 }} name='infocirlceo' size={20} color='white' />
                                </View>
                                </TouchableWithoutFeedback>
                                <View style={{ margin: 10 }}>
                                    <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#5e5d5d' }}>Dr. Uday Shetty</Text>
                                    <Text style={{ fontSize: 11, color: 'grey' }}>General Medicine</Text>
                                    <Text style={{ fontWeight: 'bold', fontSize: 13, color: '#5e5d5d' }}>Qualification</Text>
                                    <Text style={{ fontSize: 12, color: 'grey' }}>MBBS,MS</Text>
                                </View>
                            </Card>
                        </View>
                    </View>
                    <View style={{ height: 150, backgroundColor: '#edebeb', }}>
                        <RNPickerSelect
                            placeholder={placeholder}
                            style={styles}
                            onValueChange={(val) => { this.onChangeSpeci(val) }}
                            items={speciality}
                        />
                        <View
                            style={{
                                borderBottomColor: 'grey',
                                borderBottomWidth: 0.5,
                                width: wp('90%'),
                                marginTop: -10,
                                marginLeft: 20,
                                marginBottom: 10
                            }}
                        />
                        <RNPickerSelect
                            placeholder={placeholder1}
                            style={styles}
                            onValueChange={(val) => { this.onChangeLocation(val) }}
                            items={Location}
                        />
                        <View
                            style={{
                                borderBottomColor: 'grey',
                                borderBottomWidth: 0.5,
                                width: wp('90%'),
                                marginTop: -10,
                                marginLeft: 20,
                                marginBottom: 10
                            }}
                        />
                        <ScrollView
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            scrollEventThrottle={200}
                            pagingEnabled
                            decelerationRate="fast"
                        >
                            {this.state.Check1.map((i, j) => {
                                return (
                                    <View style={{ justifyContent: 'flex-start', marginTop: 15, flexDirection: 'row', }}>
                                        <CheckBox onPress={() => { this.checkChange(i) }} checked={i.check} color="#595959" />
                                        <Text style={{ marginLeft: 15, marginRight: 10, color: '#595959' }}>{i.name}</Text>
                                    </View>
                                )
                            })}
                        </ScrollView>
                    </View>
                    <Text style={{ fontWeight: 'bold', fontSize: 15, marginTop: 20, marginLeft: 20 }}>Visit Fee:</Text>
                    <Text style={{ fontWeight: 'bold', fontSize: 15, marginLeft: 20 }}>Rs.1500
                        <Text style={{ fontSize: 13, color: 'grey' }}> for a
                        </Text > 30 min
                        <Text style={{ fontSize: 13, color: 'grey' }}> session
                        </Text></Text>
                    <Text style={{ color: 'grey', fontSize: 15, marginTop: 10, marginLeft: 20 }}>Available Dates</Text>
                    <View style={styles.CardView1}>
                        <Text style={{fontWeight:'bold',fontSize:15}}>May</Text>
                        <View style={{flexDirection:'row',justifyContent:'flex-start'}}>
                        <Text style={{fontWeight:'bold',alignSelf:'center',fontSize:12,color:'#59c9c2'}}>Jump To Date</Text>
                        <Icon2 style={{marginLeft:5,alignSelf:'center',color:'#59c9c2'}} name='calendar' size={15}/>
                        </View>
                    </View>
                    <View
                            style={{
                                borderBottomColor: 'grey',
                                borderBottomWidth: 0.5,
                                width: wp('90%'),
                                marginLeft: 20,
                                marginTop:5,marginBottom:2
                            }}
                        />
                    <View style={styles.CardView1}>
                        {this.state.weekArray.map((i, j) => {
                            return (
                                <TouchableWithoutFeedback onPress={()=>{this.selectTaimeAndDate(i)}}>
                                <View style={{width:wp('10%'),backgroundColor:i.select?'#edc40e': 'white' ,marginBottom:5}}>
                                        <Text style={{ textAlign: 'center', color:i.select?'white': 'grey',alignSelf:'center',fontSize:10,marginTop:3 }}>{i.day}</Text>
                                        <Text style={{ textAlign: 'center', color:i.select?'white': 'grey',alignSelf:'center',fontSize:10,marginTop:5,marginBottom:5 }}>{i.date}</Text>
                                </View>
                                </TouchableWithoutFeedback>
                            )
                        })}

                    </View>
                    <View style={{ height: 50, backgroundColor: '#edebeb',flex:1,justifyContent:'center' }}>
                    <Text style={{ color: 'grey', fontSize: 15, marginLeft: 20 }}>Available Slots (9.00am-12.30pm,6.00pm-9.00pm)</Text>
                    </View>
                    <View style={styles.CardView}>
                        {this.state.arr.map((i, j) => {
                            return (
                                <TouchableWithoutFeedback onPress={()=>{this.selectSlote(i)}}>
                                <Card style={{ height: 40, width: 60, backgroundColor:i.status?'#edc40e': 'white' }}>
                                        <Text style={{ textAlign: 'center', color:i.status?'white': 'grey',alignSelf:'center',fontSize:10,marginTop:12 }}>{i.time}</Text>
                                </Card>
                                </TouchableWithoutFeedback>
                            )
                        })}

                    </View>
                    <Button onPress={() => { this.props.navigation.navigate('Confirmation') }} full style={styles.buttonReg}><Text>Proceed </Text></Button>
                    
                </ScrollView>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: 'white',
    },
    textHeader: {
        alignSelf: 'center',
        fontSize: 18,
        fontWeight: 'bold'
    },
    viewTop: {
        alignSelf: 'center',
        width: wp('92%'),
        justifyContent: 'space-between',
        flexDirection: 'row', marginTop: 10, marginBottom: 10
    },
    viewTop1: {
        alignSelf: 'center',
        width: wp('90%'),
        justifyContent: 'flex-start',
        flexDirection: 'row',
        marginTop: 10
    },
    card: {
        width: wp('70%'),
        height: 100,
        backgroundColor: '#d0f7f1'
        // borderRadius: 6
    },
    img: {
        height: 90, width: wp('20%'), marginTop: 7
    },
    headerView: {
        justifyContent: 'space-between', flexDirection: 'row', width: wp('93%'), alignSelf: 'center', marginTop: 10
    },
    img1: {
        height: 40, width: 40, borderRadius: 20, alignSelf: 'center', backgroundColor: 'white'
    },
    text: {
        marginLeft: 20, color: 'white', fontSize: 18
    },
    viewTop2: {
        alignSelf: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    viewTop3: {
        alignSelf: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginTop: 10
    },
  
    inputAndroid: {
        fontSize: 16,
        paddingHorizontal: 10,
        paddingVertical: 8,
        borderEndWidth: 0.5,
        borderEndColor: 'grey',
        borderRadius: 8,
        width: wp('95%'),
        marginLeft: 20,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    CardView: {
        justifyContent: 'flex-start', flexDirection: 'row', width: wp('90%'), alignSelf: 'center', marginTop: 5,flexWrap:'wrap'
    },
    CardView1: {
        justifyContent: 'space-between', flexDirection: 'row', width: wp('90%'), alignSelf: 'center',flexWrap:'wrap'
    },
    buttonReg: {
        width: wp('90%'),
        alignSelf: 'center',
        backgroundColor:'#59c9c2',
        marginBottom:15,marginTop:15
    },
    
});
