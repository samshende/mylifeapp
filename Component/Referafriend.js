
import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';
import Icon from 'react-native-vector-icons/Feather'
import Icon1 from 'react-native-vector-icons/FontAwesome'
import IconMul from 'react-native-vector-icons/Entypo'
export default class Referafriend extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }


    render() {
        return (
            <Container>
                <Header style={styles.header}>
                    <Left>
                        <Icon name='menu' size={30} />
                    </Left>
                    <Body>
                        <Text style={{ textAlign: 'center' }}>Refer</Text>
                    </Body>
                    <Right>
                        <Icon1 onPress={() => { this.props.navigation.navigate('Notifications') }} style={styles.icon} color='#7a7979' name='bell' size={30} />
                        <Icon1 style={styles.icon} color='#7a7979' name='user-circle' size={30} />
                    </Right>
                </Header>
                <View style={styles.view}>
                    <Image
                        resizeMode='cover'
                        style={styles.img}
                        source={require('../Public/img/shair.jpeg')}
                    />
                    <Image
                        resizeMode='cover'
                        style={styles.img1}
                        source={require('../Public/img/shair.jpeg')}
                    />
                </View>
                <Text style={styles.text}>CLUSR123</Text>
                <Text style={styles.text1}>
                    Well organized and easy to understand Web building tutorials with lots of examples of how to use HTML, CSS, JavaScript, SQL, PHP, Python, Bootstrap, Java ...Well organized and easy to understand Web building tutorials with lots of examples of how to use HTML, CSS, JavaScript, SQL, PHP, Python, Bootstrap, Java ...
                </Text>
                <Text style={{ textAlign: 'center', marginTop: 10 }}>Share Via</Text>

                <View style={styles.iconView}>

                    <IconMul style={styles.icon11} name='facebook-with-circle' size={30} />
                    <IconMul style={styles.icon11} name='linkedin-with-circle' size={30} />
                    <IconMul style={styles.icon11} name='pinterest-with-circle' size={30} />
                    <IconMul style={styles.icon11} name='instagram-with-circle' size={30} />
                    <IconMul style={styles.icon11} name='youtube-with-circle' size={30} />

                </View>

            </Container>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: 'white'
    },
    img: {
        height: 150,
        width: 120,
        marginTop: 50
    },
    img1: {
        height: 150,
        width: 120
    },
    icon: {
        marginLeft: 15
    },
    view: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginTop: 70,
        alignSelf: 'center',
        width: wp('85%')
    },
    text: {
        alignSelf: 'center',
        color: '#2a6c9c',
        fontSize: 35,
        fontWeight: 'bold',
        marginTop: 20
    },
    text1: {
        width: wp('90%'),
        alignSelf: 'center',
        textAlign: 'center',
        fontSize: 8,
        marginTop: 10
    },
    iconView: {
        alignSelf: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginTop: 20
    },
    icon11: {
        margin: 5
    }
});
