
import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign'
import Icon1 from 'react-native-vector-icons/FontAwesome'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';

const arr=[1,2,3,4]

export default class CaptureVDevice extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action:false
        };
    }


    render() {
        return (
            <Container style={{}}>
                    <View style={{backgroundColor:'#707373',width:wp('100%'),height:70}}>
                    <View style={styles.headerView}>
                            <View style={{justifyContent:'flex-start',flexDirection:'row',alignSelf:'center'}}>
                            <Icon onPress={() => { this.props.navigation.navigate('CaptureVitals') }} name='arrowleft' size={25} color='white' />
                            <Text style={styles.text}>Capture Vitals</Text>
                        </View>
                        <Thumbnail style={styles.img} large  source={require('../Public/img/clinic-logo-png.png')} />
                    </View>
                    </View>
                    <ScrollView>
                    <Text style={styles.text1}>List of Available Devices</Text>
                    <View style={styles.viewscroll}>
                        {arr.map((i,j)=>{
                            return(
                                <Card style={styles.card}>
                                    <View style={styles.cardView}>
                                        <Text  style={styles.textc}>Test Name</Text>
                                        <View style={styles.viewC}/>
                                        <Text style={styles.textc1}>A button means an operation (or a series of operations)</Text>
                                        <Text onPress={() => { this.props.navigation.navigate('PairDevice') }} style={styles.textc11}>Take Test</Text>
                                        <Text style={styles.textc11}>Purchase</Text>


                                    </View>

                                </Card>
                            )
                        })}

                    </View>
                    </ScrollView>
              </Container>
        );
    }
}

const styles = StyleSheet.create({
    headerView:{
        justifyContent:'space-between',flexDirection:'row',width:wp('93%'),alignSelf:'center',marginTop:10
    },
    img:{
        height:40,width:40,borderRadius:20,alignSelf:'center',backgroundColor:'white'
    },
    text:{
        marginLeft:20,color:'white',fontWeight:'bold',fontSize:18
    },
    text1:{
        alignSelf:'flex-start',fontSize:17,fontWeight:'900',marginTop:30,marginBottom:20,marginLeft:10
    },
    mapView:{
        justifyContent:'flex-start',flexDirection:'row',width:wp('90%'),flexWrap:'wrap',alignSelf:'center'
    },
    view:{
        height:60,width:60,borderRadius:30,backgroundColor:'white',alignSelf:'center',margin:10
    },
    viewscroll:{
        alignSelf:'center',
        width:wp('94%'),
        justifyContent:'space-between',
        flexDirection:'row',
        flexWrap:'wrap'
    },
    card:{
        width:wp('45%'),
        height:hp('37%'),

    },
    cardView:{
        justifyContent:'center',flex:1,
        alignItems:'center',alignContent:'center',margin:5
    },
    viewC:{
        width:wp('30%'),
        height:hp('15%'),
        backgroundColor:'#cccfcf'
    },
    textc:{
            textAlign:'center',marginBottom:10,color:'#45403d'
    },
    textc1:{
        textAlign:'center',fontSize:12,marginBottom:5,marginTop:5
    },
    textc11:{
        color:'#7d708a',
        textAlign:'center'
    }
   
   
});
