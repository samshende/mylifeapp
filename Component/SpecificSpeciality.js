// SpecificSpeciality
import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    ScrollView
} from 'react-native';
import Icon2 from 'react-native-vector-icons/AntDesign'
import IconUser from 'react-native-vector-icons/Fontisto'
import IconLoc from 'react-native-vector-icons/Entypo'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs, Icon
} from 'native-base';

const Check1 = ['Available today', 'Within 5 km', 'Under 10 km']
const Hosp = [
    {
        name: 'K.M. Bhatia Hospital',
        add: 'Tulsi Green, Laxman Nagar, Baner, Pune, Maharashtra 411045'
    },
    {
        name: 'R.D. Bhatia Hospital',
        add: 'Pimple Saudagar, Pune, Maharashtra 411027'
    }
]
export default class SpecificSpeciality extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action: false
        };
    }

    render() {
        return (
            <Container>
                <View style={{ backgroundColor: '#707373', width: wp('100%'), height: 70 }}>
                    <View style={styles.headerView}>
                        <View style={{ justifyContent: 'flex-start', flexDirection: 'row', alignSelf: 'center' }}>
                            <Icon2 onPress={() => { this.props.navigation.navigate('BookAppointment') }} name='arrowleft' size={25} color='white' />
                            <Text style={styles.text11}>Book An Appointment</Text>
                        </View>
                        <Thumbnail style={styles.img} large source={require('../Public/img/clinic-logo-png.png')} />
                    </View>
                </View>
                <ScrollView>
                <Text style={{ alignSelf: 'center', margin: 10, marginBottom: 0, fontSize: 17, fontWeight: 'bold' }}><Text style={{ color: '#949492', fontSize: 16 }}>Tardeo ,Mumbai Center,Mumbai,400034...</Text>edit</Text>
                <Item style={{ alignSelf: 'center', width: wp('95%') }}>
                    <Input
                        placeholderTextColor='#9e9c98'
                        value='Bhatia'
                        placeholder='Search Doctor,Speciality,Hospital,CliniQs' />
                    <Icon2 name='search1' size={20} color='black' />
                </Item>
                    <ScrollView
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        scrollEventThrottle={200}
                        pagingEnabled
                        decelerationRate="fast"
                    >
                        {Check1.map((i, j) => {
                            return (
                                <View style={{ justifyContent: 'flex-start', width: wp('45%'), marginTop: 15, flexDirection: 'row', }}>
                                    <CheckBox color="#595959" />
                                    <Text style={{ marginLeft: 20, color: '#595959' }}>{i}</Text>
                                </View>
                            )
                        })}

                    </ScrollView>
                    <Text style={{ fontSize: 15, margin: 15, color: '#595959' }}>Search Results (1 Doctor,2 Hospitals,1 CliniQ)</Text>

                    <Text style={{ fontSize: 15, margin: 15, color: '#595959' }}>Doctors (1 Result)</Text>

                    <Card style={styles.card}>
                        <View style={styles.viewcard}>
                            <View style={{ justifyContent: 'space-between', width: wp('85%'), flexDirection: 'row' }}>
                                <View style={{ justifyContent: 'flex-start', flexDirection: 'row', width: wp('50%') }}>
                                    <Thumbnail style={styles.img11} large source={require('../Public/img/doc3.jpeg')} />
                                    <View style={{ marginLeft: 20, alignSelf: 'center' }}>
                                        <Text style={{ fontWeight: 'bold', color: '#595959' }}>Dr.Rajesh Bhatia</Text>
                                        <Text style={{ color: '#b5b5b5' }}>MBBS M.Pharm</Text>
                                    </View>
                                    <View style={{ marginLeft: 20, alignSelf: 'center' }}>
                                        <Text style={{ alignSelf: 'center', fontWeight: 'bold', color: '#595959' }}>Visit Charges</Text>
                                        <Text style={{ alignSelf: 'center', color: '#b5b5b5', fontSize: 13 }}>Rs.1000</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={{ justifyContent: 'space-between', width: wp('90%'), flexDirection: 'row', marginTop: 15 }}>
                                <View style={{ alignSelf: 'center' }}>
                                    <Text style={{ alignSelf: 'center', color: '#b5b5b5', fontSize: 14 }}>Reg.No</Text>
                                    <Text style={{ alignSelf: 'center', color: '#b5b5b5', fontSize: 14 }}>12356</Text>
                                </View>
                                <View style={{ alignSelf: 'center' }}>
                                    <Text style={{ alignSelf: 'center', color: '#b5b5b5', fontSize: 15 }}>Available on</Text>
                                    <Text style={{ alignSelf: 'center', fontWeight: 'bold', color: '#595959', fontSize: 13 }}>Mon,May 14, 9.00 am</Text>
                                </View>
                                <View style={{ alignSelf: 'center' }}>
                                    <IconLoc name='location-pin' size={20} style={{ alignSelf: 'center' }} />
                                    <Text style={{ alignSelf: 'center', color: '#b5b5b5', fontSize: 13 }}>Bhatia Hospital 1.2km</Text>
                                </View>
                            </View>
                            <View style={{ justifyContent: 'space-between', width: wp('90%'), flexDirection: 'row', marginTop: 15, alignSelf: 'center' }}>
                                <Button style={{ borderRadius: 10, backgroundColor: '#32c9ba' }}><Text>Contact Clinic</Text></Button>
                                <Button onPress={() => { this.props.navigation.navigate('BookApp') }} style={{ borderRadius: 10, backgroundColor: '#4e51ad' }}><Text>Book Appointment</Text></Button>
                            </View>

                        </View>
                    </Card>

                    <Text style={{ fontSize: 15, margin: 15, color: '#595959' }}>Hospitals (2 Results)</Text>
                    {Hosp.map((i, j) => {
                        return (

                            <Card style={styles.card1}>
                                <View style={{ margin: 10 }}>
                                    <View style={{ justifyContent: 'flex-start', flexDirection: 'row', }}>
                                        <Thumbnail style={styles.img11} large source={require('../Public/img/nb.jpeg')} />
                                        <View style={{ marginLeft: 20, alignSelf: 'center' }}>
                                            <Text style={{ fontWeight: 'bold', color: '#595959' }}>{i.name}</Text>
                                            <Text style={{ color: '#b5b5b5', fontSize: 12, width: wp('70%') }}>{i.add}</Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={{ justifyContent: 'space-between', width: wp('90%'), flexDirection: 'row', marginTop: 15, alignSelf: 'center' }}>
                                    <Button style={{ borderRadius: 10, backgroundColor: '#32c9ba', width: wp('40%') }} full><Text style={{ textAlign: 'center' }}>Contact Hospital</Text></Button>
                                    <Button onPress={() => { this.props.navigation.navigate('SearchSpeciality') }} style={{ borderRadius: 10, backgroundColor: '#4e51ad', width: wp('40%') }} full><Text style={{ textAlign: 'center' }}>View Visiting Doctors</Text></Button>
                                </View>

                            </Card>

                        )
                    })}

                    <Text style={{ fontSize: 15, margin: 15, color: '#595959' }}>CliniQ (1 Result)</Text>
                    <Card style={styles.card1}>
                        <View style={{ margin: 10 }}>
                            <View style={{ justifyContent: 'flex-start', flexDirection: 'row', }}>
                                <Thumbnail style={styles.img11} large source={require('../Public/img/nb.jpeg')} />
                                <View style={{ marginLeft: 20, alignSelf: 'center' }}>
                                    <Text style={{ fontWeight: 'bold', color: '#595959' }}>N.S CliniQ</Text>
                                    <Text style={{ color: '#b5b5b5', fontSize: 12, width: wp('70%') }}>Tulsi Green, Laxman Nagar, Baner, Pune, Maharashtra 411045</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ justifyContent: 'space-between', width: wp('90%'), flexDirection: 'row', marginTop: 15, alignSelf: 'center' }}>
                            <Button style={{ borderRadius: 10, backgroundColor: '#32c9ba', width: wp('40%') }} full><Text style={{ textAlign: 'center' }}>Contact CloniQ</Text></Button>
                            <Button onPress={() => { this.props.navigation.navigate('SearchSpeciality') }} style={{ borderRadius: 10, backgroundColor: '#4e51ad', width: wp('40%') }} full><Text style={{ textAlign: 'center' }}>View Visiting Doctors</Text></Button>
                        </View>
                    </Card>



                </ScrollView>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    headerView: {
        justifyContent: 'space-between', flexDirection: 'row', width: wp('93%'), alignSelf: 'center', marginTop: 10
    },
    img: {
        height: 40, width: 40, borderRadius: 20, alignSelf: 'center', backgroundColor: 'white'
    },
    header: {
        backgroundColor: 'white'
    },

    icon: {
        marginLeft: 15
    },
    text: {
        color: '#67547d',
        marginTop: 20, width: wp('25%')
    },
    mainView: {
        justifyContent: 'flex-start', flexDirection: 'row', width: wp('95%'), alignSelf: 'center', marginBottom: 10
    },

    img1: {
        height: 70, width: 70, borderRadius: 35, backgroundColor: 'white'
    },
    text11: {
        marginLeft: 20, color: 'white', fontWeight: 'bold', fontSize: 18
    },
    viewMap: {
        backgroundColor: 'grey', borderStyle: 'solid', height: 60, width: 60, borderRadius: 30, justifyContent: 'center', alignSelf: 'center', marginTop: 10
    },
    viewSpec: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: wp('94%'),
        alignSelf: 'center'
    },
    cardSpec: {
        width: wp('45%'),
        height: 70,
        borderRadius: 10
    },
    viewSpec1: {
        justifyContent: 'flex-start',
        flexDirection: 'row',
        margin: 10
    },
    card: {
        alignSelf: 'center',
        width: wp('95%'),
        height: 200,
        borderRadius: 10
    },
    card1: {
        alignSelf: 'center',
        width: wp('95%'),
        height: 150,
        borderRadius: 10
    },
    viewcard: {
        margin: 10
    },
    img11: {
        height: 50, width: 50, borderRadius: 25, alignSelf: 'center', backgroundColor: 'white'
    },

});
