import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather'
import Icon1 from 'react-native-vector-icons/FontAwesome'
import IconUser from 'react-native-vector-icons/FontAwesome5'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';

const familyArr = [
    1, 2, 3, 4, 5
]
const EmeArr = [1, 2, 3]
const arr = [
    {
        Key: 0,
        name: "About CliniQ"
    },
    {
        Key: 1,
        name: "Privacy Policy"
    },
    {
        Key: 2,
        name: "Terms and Conditions"
    },
    {
        Key: 3,
        name: "FAQs And Support"
    },


]
export default class MyAccount extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action: false
        };
    }
    // ViewInsurance
    // AddInsurance

    render() {
        return (
            <Container>
                <Header style={styles.header}>
                    <Left>
                        <Icon name='menu' size={30} />
                    </Left>
                    <Body>
                        <Text style={{ textAlign: 'center' }}>My Account</Text>
                    </Body>
                    <Right>
                        <Icon1 onPress={() => { this.props.navigation.navigate('Notifications') }} style={styles.icon} color='#7a7979' name='bell' size={30} />
                        <Icon1 style={styles.icon} color='#7a7979' name='user-circle' size={30} />
                    </Right>
                </Header>
                <ScrollView>
                    <View style={styles.proView}>
                        <View style={styles.viewProfile}>
                            <Thumbnail style={styles.img} large source={require('../Public/img/doc3.jpeg')} />
                            <Text style={{ alignSelf: 'center', fontSize: 16, fontWeight: 'bold' }}>Uday Shetty</Text>
                        </View>
                        <Text onPress={() => { this.props.navigation.navigate('MyProfile') }} style={{ alignSelf: 'center', color: '#b260e0' }}>Edit</Text>

                    </View>

                    <View style={styles.viewS}>
                        <Text style={{ fontSize: 15, fontWeight: 'bold', color: '#707373' }}>My Family</Text>
                        <Text onPress={() => { this.props.navigation.navigate('MyFamily') }} style={{ color: '#b260e0' }}>Manage</Text>
                    </View>
                    <ScrollView
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        scrollEventThrottle={200}
                        pagingEnabled
                        decelerationRate="fast"
                    >
                        {familyArr.map((i, j) => {
                            return (
                                <View style={styles.mapV}>
                                    <View style={styles.viewMap}>
                                        <IconUser name='user-alt' size={40} color='white' />
                                    </View>
                                    <Text style={{ color: 'grey' }}>Member</Text>
                                    <Text style={{ color: 'grey' }}>Name</Text>
                                </View>

                            )
                        })}


                    </ScrollView>

                    <View style={styles.viewS}>
                        <Text style={{ fontSize: 15, fontWeight: 'bold', color: '#707373' }}>My Emergency Contacts</Text>
                        <Text onPress={() => { this.props.navigation.navigate('EmergencyContact') }} style={{ color: '#b260e0' }}>Manage</Text>
                    </View>
                    <ScrollView
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        scrollEventThrottle={200}
                        pagingEnabled
                        decelerationRate="fast"
                    >
                        {EmeArr.map((i, j) => {
                            return (
                                <View >
                                    <View style={styles.viewMap}>
                                        <IconUser name='user-alt' size={40} color='white' />
                                    </View>
                                    <Text style={{ color: 'grey', marginLeft: 20 }}>Name</Text>
                                    <Text style={{ color: 'grey', marginLeft: 20 }}>Contact number</Text>
                                </View>

                            )
                        })}


                    </ScrollView>
                    <View style={styles.mainView}>
                        {arr.map((i, j) => {
                            return (
                                <Text style={{ fontSize: 15, fontWeight: 'bold', color: '#707373', marginBottom: 13 }}>{i.name}</Text>
                            )
                        })}

                    </View>
                    <Button onPress={() => { this.props.navigation.navigate('MyReport') }} full style={styles.buttonReg}><Text>Logout</Text></Button>
                    <Text style={{marginBottom:20,color:'grey',alignSelf:'center'}}>App Version</Text>
                </ScrollView>
            </Container>
        );
    }
}

const styles = StyleSheet.create({

    header: {
        backgroundColor: 'white'
    },

    icon: {
        marginLeft: 15
    },
    text: {
        color: '#b260e0',
        marginTop: 20, marginLeft: 20, marginBottom: 20
    },
    viewS: {
        justifyContent: 'space-between', width: wp('90%'), alignSelf: 'center', flexDirection: 'row', marginTop: 20, marginBottom: 10
    },
    img: {
        height: 70, width: 70, borderRadius: 35, alignSelf: 'center', backgroundColor: 'white'
    },
    viewMap: {
        backgroundColor: 'grey', borderStyle: 'solid', height: 60, width: 60, borderRadius: 30, justifyContent: 'center', flex: 1, alignItems: 'center', marginTop: 10, marginLeft: 20, marginBottom: 10
    },
    mapV: {
        justifyContent: 'center', flex: 1, alignItems: 'center', alignContent: 'center'
    },
    mainView: {
        margin: 20, marginTop: 50, marginBottom: 20
    },
    buttonReg: {
        width: wp('90%'),
        alignSelf: 'center',
        backgroundColor: '#707373',
        marginBottom: 15
    },
    proView: {
        width: wp('90%'),
        alignSelf: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop:20

    },
    viewProfile: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: wp('50%')
    }

});
