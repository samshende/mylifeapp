
import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign'
import Icon1 from 'react-native-vector-icons/FontAwesome'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';

const arr=[1,4]

export default class PairDevice extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action:false
        };
    }


    render() {
        return (
            <Container style={{}}>
                    <View style={{backgroundColor:'#707373',width:wp('100%'),height:70}}>
                    <View style={styles.headerView}>
                            <View style={{justifyContent:'flex-start',flexDirection:'row',alignSelf:'center'}}>
                            <Icon onPress={() => { this.props.navigation.navigate('CaptureVDevice') }} name='arrowleft' size={25} color='white' />
                            <Text style={styles.text}>Capture Vitals</Text>
                        </View>
                        <Thumbnail style={styles.img} large  source={require('../Public/img/clinic-logo-png.png')} />
                    </View>
                    </View>
                    <ScrollView>
                        <View style={styles.scrollview}>
                    <Text style={styles.text1}>List of Available Devices</Text>
                    <Icon name='reload1' size={25} color='#636260'/>
                    </View>
                    <View style={styles.viewscroll}>
                        {arr.map((i,j)=>{
                            return(
                             <Card style={styles.card}>
                                 <View style={styles.cardView}>
                                     <View style={styles.circle}/>
                                     <View>
                                         <Text style={{fontSize:16,fontWeight:'bold',color:'#78797a'}}>Device Name</Text>
                                         <Text style={styles.textcard}>Device ID</Text>
                                         <Text style={{width:wp('40%'),color:'#95999c',fontSize:12}}>A button means an operation (or a series of operations)</Text>
                                     </View>
                                     <View style={styles.square}>
                                         <Text onPress={() => { this.props.navigation.navigate('PairingDevice') }}
                                          style={{alignSelf:'center',color:'white'}}>Pair</Text>
                                         </View>

                                 </View>

                             </Card>
                            )
                        })}

                    </View>
                    </ScrollView>
              </Container>
        );
    }
}

const styles = StyleSheet.create({
    headerView:{
        justifyContent:'space-between',flexDirection:'row',width:wp('93%'),alignSelf:'center',marginTop:10
    },
    img:{
        height:40,width:40,borderRadius:20,alignSelf:'center',backgroundColor:'white'
    },
    text:{
        marginLeft:20,color:'white',fontWeight:'bold',fontSize:18
    },
    text1:{
        fontSize:17,fontWeight:'bold',color:'#78797a'
    },
    mapView:{
        justifyContent:'flex-start',flexDirection:'row',width:wp('90%'),flexWrap:'wrap',alignSelf:'center'
    },
    view:{
        height:60,width:60,borderRadius:30,backgroundColor:'white',alignSelf:'center',margin:10
    },
    viewscroll:{
        alignSelf:'center',
        width:wp('94%'),
        justifyContent:'space-between',
        flexDirection:'row',
        flexWrap:'wrap'
    },
    card:{
        width:wp('92%'),
        height:hp('20%'),
        borderRadius:5

    },
    cardView:{
        justifyContent:'space-between',
        alignItems:'center',alignContent:'center',flexDirection:'row',flex:1,width:wp('87%'),alignSelf:'center'
    },
    viewC:{
        width:wp('30%'),
        height:hp('15%'),
        backgroundColor:'red'
    },
    textc:{
            textAlign:'center',marginBottom:10
    },
    textc1:{
        textAlign:'center',fontSize:12,marginBottom:5,marginTop:5
    },
    scrollview:{
        justifyContent:'space-between',flexDirection:'row',width:wp('90%'),alignSelf:'center',marginTop:40,marginBottom:20
    },
   circle:{
       height:70,width:70,borderRadius:35,backgroundColor:'#a17167'

   },
   square:{
       height:70,width:70,borderRadius:10,backgroundColor:'#547294',justifyContent:'center'
   },
   textcard:{
       color:'#95999c',fontSize:12
   }
   
});
