
import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    ScrollView,
    TouchableWithoutFeedback
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather'
import Icon1 from 'react-native-vector-icons/FontAwesome'
import Icon2 from 'react-native-vector-icons/AntDesign'


import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';

const arr = [1, 2, 3, 4]
export default class BookApp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action: false,

        };
    }


    render() {

        return (
            <Container style={{}}>
                <View style={{ backgroundColor: '#2a85c7', width: wp('100%'), height: 70 }}>
                    <View style={styles.headerView}>
                        <View style={{ justifyContent: 'flex-start', flexDirection: 'row', alignSelf: 'center' }}>
                            <Icon2 onPress={() => { this.props.navigation.navigate('BookApp') }} name='arrowleft' size={25} color='white' />
                            <Text style={styles.text}>Book An Appointment</Text>
                        </View>
                        <Thumbnail style={styles.img1} large source={require('../Public/img/clinic-logo-png.png')} />
                    </View>
                </View>
                <ScrollView style={{ backgroundColor: '#59c9c2' }}>
                    <View style={{ backgroundColor: 'white', height: 20 }} />
                    <Image
                        // resizeMode='contain'
                        style={styles.img}
                        source={require('../Public/img/doc.jpeg')}
                    />
                    <Card style={styles.card}>
                        <View style={{ backgroundColor: 'white', height: 55 }}>
                            <Text style={{ marginTop: 7, textAlign: 'center', fontSize: 20, color: 'grey', fontWeight: 'bold' }}>Dr.Uday Shetty</Text>
                            <Text style={{ textAlign: 'center', fontSize: 11, color: 'grey', marginBottom: 3, marginTop: 2 }}>General Medicine</Text>
                            <View style={{ justifyContent: 'space-between', alignSelf: 'center', flexDirection: 'row', width: wp('90%'), marginTop: 15 }}>
                                <View style={styles.subview}>
                                    <Button onPress={() => { this.props.navigation.navigate('Confirmation') }} full style={styles.buttonReg}>
                                        <Text style={{ textAlign: 'center' }}>Book Appointment </Text>
                                    </Button>
                                    <Text style={{ fontSize: 14, fontWeight: 'bold', marginTop: 10 }}>Area Of Expertise</Text>
                                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                                        <View style={{ height: 6, width: 6, borderRadius: 3, backgroundColor: 'black', alignSelf: 'center' }} />
                                        <Text style={{ marginLeft: 10, color: 'grey' }}>ENT</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                                        <View style={{ height: 6, width: 6, borderRadius: 3, backgroundColor: 'black', alignSelf: 'center' }} />
                                        <Text style={{ marginLeft: 10, color: 'grey' }}>ENT</Text>
                                    </View>
                                    <Text style={{ fontSize: 14, fontWeight: 'bold', marginTop: 15 }}>Qualification</Text>
                                    <Text style={{ fontSize: 14, color: 'grey', marginBottom: 10 }}>MBBS,MS</Text>
                                </View>
                                <View style={styles.subview}>
                                    <Button onPress={() => { this.props.navigation.navigate('Confirmation') }} full style={styles.buttonReg1}>
                                        <Text style={{ textAlign: 'center' }}>Contact Doctor </Text>
                                    </Button>
                                    <Text style={{ fontSize: 14, fontWeight: 'bold', marginTop: 10 }}></Text>
                                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                                        <View style={{ height: 6, width: 6, borderRadius: 3, backgroundColor: 'black', alignSelf: 'center' }} />
                                        <Text style={{ marginLeft: 10, color: 'grey' }}>ENT</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                                        <View style={{ height: 6, width: 6, borderRadius: 3, backgroundColor: 'black', alignSelf: 'center' }} />
                                        <Text style={{ marginLeft: 10, color: 'grey' }}>ENT</Text>
                                    </View>
                                    <Text style={{ fontSize: 14, fontWeight: 'bold', marginTop: 15 }}>Year of Practice</Text>
                                    <Text style={{ fontSize: 14, color: 'grey', marginBottom: 10 }}>B+ Year</Text>

                                </View>

                            </View>
                            <View style={{ alignSelf: 'center', width: wp('90%'), }}>
                                <Text style={{ fontSize: 14, fontWeight: 'bold', marginTop: 15 }}>Visit Fee:</Text>
                                <Text style={{ fontSize: 14, color: 'grey' }}>New Appointment  <Text style={{ fontSize: 14, fontWeight: 'bold' }}>Rs.1500 </Text>for a <Text style={{ fontSize: 14, fontWeight: 'bold' }}>30 min </Text>session</Text>
                                <Text style={{ fontSize: 14, color: 'grey' }}>follow up                  <Text style={{ fontSize: 14, fontWeight: 'bold' }}>Rs.1500 </Text>for a <Text style={{ fontSize: 14, fontWeight: 'bold' }}>30 min </Text>session</Text>
                                <Text style={{ fontSize: 14, fontWeight: 'bold', marginTop: 15 }}>About the Doctor</Text>
                                <Text style={{ fontSize: 14, color: 'grey', marginTop: 5 }}>But i must expliain to you how all this mistaken idia of  to understand Web building tutorials </Text>
                            </View>
                        </View>
                    </Card>

                    <Card style={styles.card1}>
                        <View style={{ alignSelf: 'center', width: wp('90%'), }}>
                            <Text style={{ fontSize: 20, marginTop: 15, color: 'grey' }}>General Quations</Text>
                            <View
                                style={{
                                    borderBottomColor: 'grey',
                                    borderBottomWidth: 0.5,
                                    width: wp('70%'),
                                    marginTop: 5
                                }}
                            />
                            {arr.map((i, j) => {
                                return (
                                    <View>
                                        <Text style={{ fontSize: 14, fontWeight: 'bold', marginTop: 5 }}>What is this Quations?</Text>
                                        <Text style={{ fontSize: 14, color: 'grey', marginTop: 5, marginBottom: 5 }}>But i must expliain to you how all this mistaken idia of  to understand Web building tutorials idia of  to understand Web building tutorials </Text>
                                    </View>
                                )
                            })}

                        </View>
                    </Card>

                    <Card style={styles.card1}>
                        <View style={{ alignSelf: 'center', width: wp('90%'), }}>
                            <Text style={{ fontSize: 20, marginTop: 15, color: 'grey' }}>Reviews</Text>
                            <View
                                style={{
                                    borderBottomColor: 'grey',
                                    borderBottomWidth: 0.5,
                                    width: wp('70%'),
                                    marginTop: 5
                                }}
                            />
                            {arr.map((i, j) => {
                                return (
                                    <View>
                                        <Text style={{ fontSize: 14, color: 'grey', marginTop: 5, }}>But i must expliain to you how all this mistaken idia of  to understand Web building tutorials idia of  to understand Web building tutorials </Text>
                                        <Text style={{ fontSize: 14, fontWeight: 'bold', marginBottom: 5 }}>- John Snow</Text>
                                    </View>
                                )
                            })}

                        </View>
                        <View style={{ justifyContent: 'space-between', alignSelf: 'center', flexDirection: 'row', width: wp('90%'), }}>
                        <Text style={{ fontSize: 12, marginTop: 5, color: '#59c9c2',alignSelf:'center' }}>View More Reviews</Text>
                        <TouchableWithoutFeedback>

                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start',alignItems:'center',alignContent:'center'}}>
                            <Icon2 name='plus' color="#88bd20" size={20} />
                        <Text style={{ fontSize: 12, marginTop: 5, color: '#88bd20',marginLeft:10,}}>Add  A Reviews</Text>
                            </View>
                        </TouchableWithoutFeedback>

                            </View>
                    </Card>
                    <Button onPress={() => { this.props.navigation.navigate('BookApp') }} full style={styles.book}>
                                        <Text style={{ textAlign: 'center' }}>Book Appointment </Text>
                                    </Button>
                </ScrollView>
          
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    card: {
        width: wp('94%'), alignSelf: 'center', marginTop: -55, backgroundColor: '#d0f7f1', height: 420,
    },
    card1: {
        width: wp('94%'), alignSelf: 'center', backgroundColor: '#d0f7f1', height: 420, marginTop: 10
    },
    headerView: {
        justifyContent: 'space-between', flexDirection: 'row', width: wp('93%'), alignSelf: 'center', marginTop: 10
    },
    img1: {
        height: 40, width: 40, borderRadius: 20, alignSelf: 'center', backgroundColor: 'white'
    },
    text: {
        marginLeft: 20, color: 'white', fontSize: 18
    },
    img: {
        height: 250, width: wp('100%'), alignSelf: 'center',
    },
    buttonReg: {
        width: wp('43%'),
        alignSelf: 'center',
        backgroundColor: '#59c9c2'
    },
    buttonReg1: {
        width: wp('43%'),
        alignSelf: 'center',
        backgroundColor: '#88bd20'
    },
    subview: {
        width: wp('44%'),
    },
    book: {
        width: wp('90%'),
        alignSelf: 'center',
        backgroundColor: '#2a85c7',
        marginBottom: 15,
        marginTop: 10
    },
});
