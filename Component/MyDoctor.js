
import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather'
import Icon1 from 'react-native-vector-icons/FontAwesome'
import Icon2 from 'react-native-vector-icons/AntDesign'


import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';

const arr = [1, 4,3]

export default class MyDoctor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action: false
        };
    }


    render() {
        return (
            <Container style={{}}>
                <Header style={styles.header}>
                    <Left>
                        <Icon name='menu' size={30} />
                    </Left>
                    <Text style={styles.textHeader}>
                        Share with My Doctor
                        </Text>
                    <Right>
                        <Icon1 style={{}} color='#7a7979' name='user-circle' size={30} />
                    </Right>
                </Header>
                <ScrollView>
                    <View style={styles.viewTop}>
                        <Text style={{ color: '#67547d' }}>Filter</Text>
                        <Text style={{ color: '#67547d' }}>Search</Text>
                        <Text >Add New</Text>
                    </View>
                    {arr.map((i,j)=>{
                            return(
                    <View style={styles.viewTop1}>
                        <View style={{ width: wp('19%') }}>
                            <Image
                                resizeMode='contain'
                                style={styles.img}
                                source={require('../Public/img/doc3.jpeg')}
                            />
                        </View>
                   
                                
                        <Card style={styles.card}>
                            <View style={{position:'absolute',right:0,top:0,backgroundColor:'#19cbe3',height:30,width:30,}}>
                                <Icon2 style={{alignSelf:'center',marginTop:4}} name='infocirlceo' size={20} color='white' />
                            </View>
                            <View style={{margin:10}}>
                                <Text style={{fontWeight:'bold',fontSize:16,color:'#5e5d5d'}}>Dr. Uday Shetty</Text>
                                <Text style={{fontSize:11,color:'grey'}}>General Medicine</Text>
                                <Text style={{fontWeight:'bold',fontSize:13,color:'#5e5d5d'}}>Qualification</Text>
                                <Text style={{fontSize:12,color:'grey'}}>MBBS,MS</Text>
                                <Text style={{marginLeft:20,fontWeight:'bold',fontSize:13,color:'#5e5d5d'}}>Share Via</Text>
                                <View style={{justifyContent:'space-between',flexDirection:'row',width:wp('35%'),marginLeft:20,marginTop:5}}>
                                    <Text style={{ color: '#67547d',fontSize:13 }}>Whatsapp</Text>
                                    <Text style={{ color: '#67547d',fontSize:13 }}>Mail</Text>
                                </View>

                            </View>


                        </Card>


                    </View>
                    )
                })}

                </ScrollView>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: 'white'
    },
    textHeader: {
        alignSelf: 'center',
        fontSize: 18,
        fontWeight: 'bold'
    },
    viewTop: {
        alignSelf: 'center',
        width: wp('92%'),
        justifyContent: 'space-between',
        flexDirection: 'row', marginTop: 20, marginBottom: 20
    },
    viewTop1: {
        alignSelf: 'center',
        width: wp('90%'),
        justifyContent: 'flex-start',
        flexDirection: 'row',
        marginTop:20
    },
    card: {
        width: wp('70%'),
        height: 150,
        borderRadius: 6
    },
    img: {
        height: 100, width: wp('20%'), marginTop: 7
    }

});
