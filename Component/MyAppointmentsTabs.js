import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    ScrollView
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';

const ActionArray = [
    {
        name: 'Dr.Rajesh Bhatia',
        tag: 'Document Tag',
        doctor: 'Doctor Name',
        day: 'Tue',
        date: '24',
        month: 'May',
        time: '10.30 Am',
        key: 0
    },
    {
        name: 'Dr.Rajesh Bhatia',
        tag: 'Document Tag',
        doctor: 'Doctor Name',
        day: 'Tue',
        date: '24',
        month: 'May',
        time: '10.30 Am',
        key: 1
    },
]

export default class MyAppointmentsTabs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action: false
        };
    }
  
    render() {
        return (
            <Container>
                <ScrollView>
                    <View style={{ justifyContent: 'space-between', alignSelf: 'center', flexDirection: 'row', width: wp('90%'), marginBottom: 10 }}>
                        <Text style={{ color: '#a1a3a6', width: wp('50%'), marginTop: 20, }}>Scheduled</Text>
                        <Text style={styles.text} onPress={() => { this.props.navigation.navigate('BookAppointment') }}>Add New</Text>
                        <Text style={styles.text}>Filter</Text>
                    </View>
                    <View style={styles.mainView}>
                        <Card style={styles.card11}>
                            <View style={styles.card1view}>
                                <Text style={{ color: 'white', fontSize: 13 }}>Tue</Text>
                                <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}>24</Text>
                                <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}>Jun</Text>
                                <Text style={{ color: 'white', fontSize: 12 }}>10.30 Am</Text>
                            </View>
                        </Card>
                        <Card style={styles.card2}>
                            <Text style={{ fontSize: 12, margin: 10, color: '#6d6d6e' }}>Follow up Appointment with</Text>
                            <View style={styles.card2view}>
                                <Thumbnail style={styles.img} large source={require('../Public/img/doc3.jpeg')} />
                                <View style={{ marginLeft: 20, marginTop: 5 }}>
                                    <Text style={{ fontSize: 17, color: '#707073' }}>Dr.Rajesh Bhatia</Text>
                                    <Text style={{ fontSize: 10, color: 'grey' }}>Document Tag</Text>
                                    <Text style={{ fontSize: 10 }}>Doctor Name</Text>
                                 
                                </View>
                            </View>
                            <View style={{ justifyContent: 'space-between', flexDirection: 'row', marginBottom: 10,width:wp('55%'),alignSelf:'center',marginTop:5,marginBottom:5 }}>
                                        <Text style={{ color: '#a1a3a6' ,fontSize:12}}>Contact</Text>
                                        <Text style={{ color:'#538a53' ,fontSize:12}}>Manage</Text>

                                    </View>
                        </Card>

                    </View>

                    <View style={{ justifyContent: 'space-between', alignSelf: 'center', flexDirection: 'row', width: wp('90%'), marginBottom: 10 }}>
                        <Text style={{ color: '#a1a3a6',fontSize:15 }}>Past Appointments</Text>
                        <Text style={{ color: '#a1a3a6',fontSize:15 }}>Jump To a Date</Text>

                    </View>
                    {ActionArray.map((i, j) => {
                        return (
                            <View style={styles.mainView}>
                                <Card style={styles.card1}>
                                    <View style={styles.card1view}>
                                        <Text style={{ color: 'white', fontSize: 13 }}>{i.day}</Text>
                                        <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}>{i.date}</Text>
                                        <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}>{i.month}</Text>
                                        <Text style={{ color: 'white', fontSize: 12 }}>{i.time}</Text>
                                    </View>
                                </Card>
                                <Card style={styles.card2}>
                                    <Text style={{ fontSize: 12, margin: 10, color: '#6d6d6e' }}>New Appointment with</Text>
                                    <View style={styles.card2view}>
                                        <Thumbnail style={styles.img} large source={require('../Public/img/doc3.jpeg')} />
                                        <View style={{ marginLeft: 20, marginTop: 5 }}>
                                            <Text style={{ fontSize: 17, color: '#707073' }}>{i.name}</Text>
                                            <Text style={{ fontSize: 10, color: 'grey' }}>{i.tag}</Text>
                                            <Text style={{ fontSize: 10 }}>{i.doctor}</Text>
                                            <Text style={{ color: '#538a53', fontSize: 12, marginTop: 7 }}>view Appointment</Text>
                                        </View>
                                    </View>
                                </Card>

                            </View>
                        )

                    })}
                </ScrollView>
            </Container>
        );
    }
}

const styles = StyleSheet.create({

    header: {
        backgroundColor: 'white'
    },

    icon: {
        marginLeft: 15
    },
    text: {
        color: '#67547d',
        marginTop: 20, width: wp('25%')
    },
    mainView: {
        justifyContent: 'flex-start', flexDirection: 'row', width: wp('95%'), alignSelf: 'center', marginBottom: 10
    },
    card1: {
        width: wp('23%'),
        height: 140,
        backgroundColor: '#3d68ad'
    },
    card11: {
        width: wp('23%'),
        height: 140,
        backgroundColor: '#a6b317'
    },
    card2: {
        width: wp('70%'),
        height: 140, marginLeft: -2
    },
    card1view: {
        margin: 10,marginTop:20, marginLeft: 15
    },
    card2view: {
        justifyContent: 'flex-start',
        flexDirection: 'row',
        marginLeft: 10
    },
    img: {
        height: 70, width: 70, borderRadius: 35, backgroundColor: 'white'
    },

});
