
import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign'
import Icon1 from 'react-native-vector-icons/FontAwesome'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';

const arr = [1, 4]

export default class StartTest extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action: false
        };
    }


    render() {
        return (
            <Container style={{}}>
                <View style={{ backgroundColor: '#707373', width: wp('100%'), height: 70 }}>
                    <View style={styles.headerView}>
                        <View style={{ justifyContent: 'flex-start', flexDirection: 'row', alignSelf: 'center' }}>
                            <Icon onPress={() => { this.props.navigation.navigate('PairingDeviceS') }} name='arrowleft' size={25} color='white' />
                            <Text style={styles.text}>Capture Vitals</Text>
                        </View>
                        <Thumbnail style={styles.img} large source={require('../Public/img/clinic-logo-png.png')} />
                    </View>
                </View>
                <ScrollView>

                <Card style={styles.card}>
                    <View style={styles.cardView}>
                        <View style={styles.circle} />
                        <View style={{marginLeft:20}}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#78797a' }}>Device Name</Text>
                            <Text style={styles.textcard}>Device ID</Text>
                        </View>
                    </View>
                </Card>
                <Text style={styles.textInst}>Instructions</Text>
                <Text style={styles.textcard1}>
                    krjbfkojnbgjknbkjl ,mkl vdjblkn
                    textbottom: 
        position: 'absolute', bottom: 70, fontWeight: 'bold', color: '#95999c', fontSize: 18
                </Text>
                <Card style={styles.cardlarge}>
                    <View style={styles.cardViewlarge}>
                        <Text style={{fontSize:13}}>Device Test Proccess here</Text>
                    </View>

                </Card>
                <Button onPress={() => { this.props.navigation.navigate('TestProcessing') }} full style={styles.buttonReg}><Text>Start Test</Text></Button>

                </ScrollView>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    headerView: {
        justifyContent: 'space-between', flexDirection: 'row', width: wp('93%'), alignSelf: 'center', marginTop: 10
    },
    img: {
        height: 40, width: 40, borderRadius: 20, alignSelf: 'center', backgroundColor: 'white'
    },
    text: {
        marginLeft: 20, color: 'white', fontWeight: 'bold', fontSize: 18
    },
    scrollview: {
        justifyContent: 'space-between', flexDirection: 'row', width: wp('90%'), alignSelf: 'center', marginTop: 40, marginBottom: 20
    },
    circle:{
        height:70,width:70,borderRadius:35,backgroundColor:'#a17167'
 
    },
    textcard: {
        color: '#95999c', fontSize: 12
    },
    mainView: {
        alignItems: 'center', alignContent: 'center', flex: 1, marginTop: 30
    },
    textd: {
        fontSize: 18, fontWeight: 'bold', marginBottom: 20, color: '#78797a'
    },
    textbottom: {
        position: 'absolute', bottom: 70, fontWeight: 'bold', color: '#95999c', fontSize: 18
    },
    card:{
        width:wp('92%'),
        height:hp('17%'),
        borderRadius:5,
        alignSelf:'center',
        marginTop:30

    },
    cardView:{
        justifyContent:'flex-start',
        alignItems:'center',alignContent:'center',flexDirection:'row',flex:1,width:wp('87%'),alignSelf:'center'
    },
    textInst:{
        marginTop:7,fontSize:18,fontWeight:'bold',color: '#78797a',marginLeft:20
    },
    textcard1: {
        color: '#95999c', fontSize: 12,marginLeft:20,flex:1,marginRight:10,marginBottom:20
    },
    cardlarge:{
        width:wp('92%'),
        height:250,
        alignSelf:'center',
        borderColor:'black',marginBottom:20
    },
    cardViewlarge:{
        justifyContent:'center',flex:1,alignItems:'center'
    },
    buttonReg: {
        width: wp('90%'),
        alignSelf: 'center',
        backgroundColor:'#414240',
        marginBottom:30
    },

});
