
import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign'
import Icon1 from 'react-native-vector-icons/FontAwesome'
import IconUser from 'react-native-vector-icons/FontAwesome5'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';

const familyArr = [
    1, 2, 3,4
]

export default class MyFamily extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action:false
        };
    }


    render() {
        return (
            <Container style={{}}>
                    <View style={{backgroundColor:'#707373',width:wp('100%'),height:70}}>
                    <View style={styles.headerView}>
                            <View style={{justifyContent:'flex-start',flexDirection:'row',alignSelf:'center'}}>
                            <Icon onPress={() => { this.props.navigation.navigate('MyAccount') }} name='arrowleft' size={25} color='white' />
                            <Text style={styles.text}>My Family</Text>
                        </View>
                        <Thumbnail style={styles.img} large  source={require('../Public/img/clinic-logo-png.png')} />
                    </View>
                    </View>
                    <ScrollView>
                    {familyArr.map((i, j) => {
                            return (
                                <View style={styles.mapV}>

                                    <View style={styles.viewstart}>

                                    <View style={styles.viewMap}>
                                        <IconUser style={{alignSelf:'center'}} name='user-alt' size={40} color='white' />
                                    </View>

                                    <View style={{marginLeft:10,alignSelf:'center'}}>
                                    <Text style={{ color: 'grey' }}>Member</Text>
                                    <Text style={{ color: 'grey' }}>Name</Text>
                                    </View>

                                    </View>

                                    <Text style={{ alignSelf: 'center', color: '#b260e0' }}>Edit</Text>
                                </View>

                            )
                        })}

                        <Text style={{ alignSelf: 'center', color: '#b260e0',marginTop:50, }}>Set As current Profile???</Text>
                    </ScrollView>
                </Container>
        );
    }
}

const styles = StyleSheet.create({
    headerView:{
        justifyContent:'space-between',flexDirection:'row',width:wp('93%'),alignSelf:'center',marginTop:10
    },
    img:{
        height:40,width:40,borderRadius:20,alignSelf:'center',backgroundColor:'white'
    },
    text:{
        marginLeft:20,color:'white',fontWeight:'bold',fontSize:18
    },
    viewMap: {
        height:60,width:60,borderRadius:30,justifyContent:'center',backgroundColor:'grey'
    },
    mapV: {
        justifyContent: 'space-between', flexDirection:'row',width:wp('85%'),alignSelf:'center',marginTop:20
    },
    viewstart:{
        justifyContent:'flex-start',
        flexDirection:'row',

    }
});
