import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    ScrollView
} from 'react-native';
import Icon2 from 'react-native-vector-icons/AntDesign'
import IconUser from 'react-native-vector-icons/Fontisto'
import IconLoc from 'react-native-vector-icons/Entypo'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs, Icon
} from 'native-base';

const Check1 = ['Available today', 'Within 5 km', 'Under 10 km']
const Hosp = [
    {
        name: 'Dr. Rajesh Bhatia',
        Qualif: 'MBBS M.Pharm',
        charge: 'Rs.1000',
        RegNo: 12356,
        date: 'Mon,May 14, 9.00 am',
        loc: 'Bhatia Hospital 1.2km'
    },
    {
        name: 'Dr. Rajesh',
        Qualif: 'MBBS M.Pharm',
        charge: 'Rs.1000',
        RegNo: 12356,
        date: 'Mon,May 14, 9.00 am',
        loc: 'Bhatia Hospital 1.2km'
    },
    {
        name: 'Dr.R.R Bhatia',
        Qualif: 'MBBS M.Pharm',
        charge: 'Rs.1000',
        RegNo: 12356,
        date: 'Mon,May 14, 9.00 am',
        loc: 'Bhatia Hospital 1.2km'
    },
]
export default class SearchSpeciality extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action: false
        };
    }

    render() {
        return (
            <Container>
                <View style={{ backgroundColor: '#707373', width: wp('100%'), height: 70 }}>
                    <View style={styles.headerView}>
                        <View style={{ justifyContent: 'flex-start', flexDirection: 'row', alignSelf: 'center' }}>
                            <Icon2 onPress={() => { this.props.navigation.navigate('CaptureVitals') }} name='arrowleft' size={25} color='white' />
                            <Text style={styles.text11}>Book An Appointment</Text>
                        </View>
                        <Thumbnail style={styles.img} large source={require('../Public/img/clinic-logo-png.png')} />
                    </View>
                </View>
                <ScrollView>
                    <Text style={{ alignSelf: 'center', margin: 10, marginBottom: 0, fontSize: 17, fontWeight: 'bold' }}><Text style={{ color: '#949492', fontSize: 16 }}>Tardeo ,Mumbai Center,Mumbai,400034...</Text>edit</Text>
                    <Item style={{ alignSelf: 'center', width: wp('95%') }}>
                        <Input
                            placeholderTextColor='#9e9c98'
                            placeholder='Search Specialist' />
                        <Icon2 name='search1' size={20} color='black' />
                    </Item>
                    <ScrollView
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        scrollEventThrottle={200}
                        pagingEnabled
                        decelerationRate="fast"
                    >
                        {Check1.map((i, j) => {
                            return (
                                <View style={{ justifyContent: 'flex-start', width: wp('45%'), marginTop: 15, flexDirection: 'row', }}>
                                    <CheckBox color="#595959" />
                                    <Text style={{ marginLeft: 20, color: '#595959' }}>{i}</Text>
                                </View>
                            )
                        })}

                    </ScrollView>
                    <View
                        style={{
                            borderBottomColor: 'grey',
                            borderBottomWidth: 1,
                            width:wp('90%'),
                            alignSelf:'center',
                            marginTop:10
                        }}
                    />
                    <Text style={{ margin: 10, color: '#595959' }}>Available in Your Area</Text>

                    {Hosp.map((i, j) => {
                        return (

                            <Card style={styles.card}>
                                <View style={styles.viewcard}>
                                    <View style={{ justifyContent: 'space-between', width: wp('85%'), flexDirection: 'row' }}>
                                        <View style={{ justifyContent: 'flex-start', flexDirection: 'row', width: wp('50%') }}>
                                            <Thumbnail style={styles.img11} large source={require('../Public/img/doc3.jpeg')} />
                                            <View style={{ marginLeft: 20, alignSelf: 'center' }}>
                                                <Text style={{ fontWeight: 'bold', color: '#595959' }}>{i.name}</Text>
                                                <Text style={{ color: '#b5b5b5' }}>{i.Qualif}</Text>
                                            </View>
                                            <View style={{ marginLeft: 20, alignSelf: 'center' }}>
                                                <Text style={{ alignSelf: 'center', fontWeight: 'bold', color: '#595959' }}>Visit Charges</Text>
                                                <Text style={{ alignSelf: 'center', color: '#b5b5b5', fontSize: 13 }}>{i.charge}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ justifyContent: 'space-between', width: wp('90%'), flexDirection: 'row', marginTop: 15 }}>
                                        <View style={{ alignSelf: 'center' }}>
                                            <Text style={{ alignSelf: 'center', color: '#b5b5b5', fontSize: 14 }}>Reg.No</Text>
                                            <Text style={{ alignSelf: 'center', color: '#b5b5b5', fontSize: 14 }}>{i.RegNo}</Text>
                                        </View>
                                        <View style={{ alignSelf: 'center' }}>
                                            <Text style={{ alignSelf: 'center', color: '#b5b5b5', fontSize: 15 }}>Available on</Text>
                                            <Text style={{ alignSelf: 'center', fontWeight: 'bold', color: '#595959', fontSize: 13 }}>{i.date}</Text>
                                        </View>
                                        <View style={{ alignSelf: 'center' }}>
                                            <IconLoc name='location-pin' size={20} style={{ alignSelf: 'center' }} />
                                            <Text style={{ alignSelf: 'center', color: '#b5b5b5', fontSize: 13 }}>{i.loc}</Text>
                                        </View>
                                    </View>
                                    <View style={{ justifyContent: 'space-between', width: wp('90%'), flexDirection: 'row', marginTop: 15, alignSelf: 'center' }}>
                                        <Button style={{ borderRadius: 10, backgroundColor: '#32c9ba' }}><Text>Contact Clinic</Text></Button>
                                        <Button onPress={() => { this.props.navigation.navigate('BookApp') }} style={{ borderRadius: 10, backgroundColor: '#4e51ad' }}><Text>Book Appointment</Text></Button>
                                    </View>

                                </View>
                            </Card>

                        )
                    })}


                </ScrollView>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    headerView: {
        justifyContent: 'space-between', flexDirection: 'row', width: wp('93%'), alignSelf: 'center', marginTop: 10
    },
    img: {
        height: 40, width: 40, borderRadius: 20, alignSelf: 'center', backgroundColor: 'white'
    },
    header: {
        backgroundColor: 'white'
    },

    icon: {
        marginLeft: 15
    },
    text: {
        color: '#67547d',
        marginTop: 20, width: wp('25%')
    },
    mainView: {
        justifyContent: 'flex-start', flexDirection: 'row', width: wp('95%'), alignSelf: 'center', marginBottom: 10
    },

    img1: {
        height: 70, width: 70, borderRadius: 35, backgroundColor: 'white'
    },
    text11: {
        marginLeft: 20, color: 'white', fontWeight: 'bold', fontSize: 18
    },
    viewMap: {
        backgroundColor: 'grey', borderStyle: 'solid', height: 60, width: 60, borderRadius: 30, justifyContent: 'center', alignSelf: 'center', marginTop: 10
    },
    viewSpec: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: wp('94%'),
        alignSelf: 'center'
    },
    cardSpec: {
        width: wp('45%'),
        height: 70,
        borderRadius: 10
    },
    viewSpec1: {
        justifyContent: 'flex-start',
        flexDirection: 'row',
        margin: 10
    },
    card: {
        alignSelf: 'center',
        width: wp('95%'),
        height: 200,
        borderRadius: 10,
        marginTop: 10
    },
    card1: {
        alignSelf: 'center',
        width: wp('95%'),
        height: 150,
        borderRadius: 10
    },
    viewcard: {
        margin: 10
    },
    img11: {
        height: 50, width: 50, borderRadius: 25, alignSelf: 'center', backgroundColor: 'white'
    },

});
