import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    ScrollView,
    TouchableWithoutFeedback
} from 'react-native';
import Icon2 from 'react-native-vector-icons/AntDesign'
import IconUser from 'react-native-vector-icons/Fontisto'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs, Icon
} from 'native-base';

const EmeArr = [1, 2, 3, 4, 5]
const Spec = [
    {
        text: 'ENT',
        text1: null
    },
    {
        text: 'Hair',
        text1: 'Specialist'
    },
    {
        text: 'Emergency',
        text1: 'Medicine'
    },
    {
        text: 'General',
        text1: 'Medicine'
    }
]

export default class BookAppointments extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action: false
        };
    }
    // ViewInsurance
    // AddInsurance

    render() {
        return (
            <Container>
                <View style={{ backgroundColor: '#707373', width: wp('100%'), height: 70 }}>
                    <View style={styles.headerView}>
                        <View style={{ justifyContent: 'flex-start', flexDirection: 'row', alignSelf: 'center' }}>
                            <Icon2 onPress={() => { this.props.navigation.navigate('HomeScreen') }} name='arrowleft' size={25} color='white' />
                            <Text style={styles.text11}>Book An Appointment</Text>
                        </View>
                        <Thumbnail style={styles.img} large source={require('../Public/img/clinic-logo-png.png')} />
                    </View>
                </View>
                <Text style={{alignSelf:'center',margin:10,marginBottom:0, fontSize:17,fontWeight:'bold'}}>Parents Home<Text style={{color:'#949492',fontSize:13}}>(Atlas apartments,baner,p...)</Text>edit</Text>
                <Item style={{alignSelf:'center',width:wp('95%')}}>
                    <Input 
                    placeholderTextColor='#9e9c98'
                    placeholder='Search Doctor,Speciality,Hospital,CliniQs' />
                    <Icon2 name='search1' size={20} color='black' />
                </Item>
                <Text style={{color:'#9e9c98',fontSize:13,margin:10}} onPress={() => { this.props.navigation.navigate('SpecificSpeciality') }}>(Click here to Search  Query entered result)</Text>
                <ScrollView>
                    <Text style={{ fontSize: 13, margin: 15 }}>My Doctrors</Text>
                    <ScrollView
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        scrollEventThrottle={200}
                        pagingEnabled
                        decelerationRate="fast"
                    >
                        {EmeArr.map((i, j) => {
                            return (
                                <TouchableWithoutFeedback onPress={() => { this.props.navigation.navigate('BookApp') }} >
                                <Card style={styles.card}>
                                    <View style={styles.viewMap}>
                                        <IconUser style={{ alignSelf: 'center' }} name='doctor' size={40} color='white' />
                                    </View>
                                    <Text style={{ fontWeight: 'bold', alignSelf: 'center', textAlign: 'center' }}>Doctor Name</Text>
                                    <Text style={{ color: 'grey', textAlign: 'center' }}>speciality</Text>
                                </Card>
                                </TouchableWithoutFeedback>
                            )
                        })}
                    </ScrollView>
                    <Text style={{ fontSize: 13, margin: 10, marginLeft: 15 }}>Search by Speciality</Text>

                    <View style={styles.viewSpec}>
                        {Spec.map((i, j) => {
                            return (
                                <TouchableWithoutFeedback onPress={() => { this.props.navigation.navigate('SearchSpeciality') }} >
                                <Card style={styles.cardSpec}>
                                    <View style={styles.viewSpec1}>
                                        <View style={{ alignSelf: 'center', width: 50, height: 50, borderRadius: 25, backgroundColor: '#a8a8a7' }} />
                                        <View style={{ justifyContent: 'center', flex: 1, }}>
                                            <Text style={{ marginLeft: 5, color: 'grey', fontSize: 14 }}>{i.text}</Text>
                                            {i.text1 != null &&
                                                <Text style={{ marginLeft: 5, color: 'grey', fontSize: 14 }}>{i.text1}</Text>
                                            }
                                        </View>
                                    </View>

                                </Card>
                                </TouchableWithoutFeedback>

                            )
                        })}

                    </View>


                </ScrollView>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    headerView: {
        justifyContent: 'space-between', flexDirection: 'row', width: wp('93%'), alignSelf: 'center', marginTop: 10
    },
    img: {
        height: 40, width: 40, borderRadius: 20, alignSelf: 'center', backgroundColor: 'white'
    },
    header: {
        backgroundColor: 'white'
    },

    icon: {
        marginLeft: 15
    },
    text: {
        color: '#67547d',
        marginTop: 20, width: wp('25%')
    },
    mainView: {
        justifyContent: 'flex-start', flexDirection: 'row', width: wp('95%'), alignSelf: 'center', marginBottom: 10
    },
    card: {
        height: 125, width: wp('32%'), marginLeft: 15
    },
    card1view: {
        margin: 10, marginTop: 20, marginLeft: 15
    },
    card2view: {
        justifyContent: 'flex-start',
        flexDirection: 'row',
        marginLeft: 10
    },
    img1: {
        height: 70, width: 70, borderRadius: 35, backgroundColor: 'white'
    },
    text11: {
        marginLeft: 20, color: 'white', fontWeight: 'bold', fontSize: 18
    },
    viewMap: {
        backgroundColor: 'grey', borderStyle: 'solid', height: 60, width: 60, borderRadius: 30, justifyContent: 'center', alignSelf: 'center', marginTop: 10
    },
    viewSpec: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: wp('94%'),
        alignSelf: 'center'
    },
    cardSpec: {
        width: wp('45%'),
        height: 70,
        borderRadius: 10
    },
    viewSpec1: {
        justifyContent: 'flex-start',
        flexDirection: 'row',
        margin: 10
    }
});
