
import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    Modal,
    TouchableWithoutFeedback,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign'
import Icon1 from 'react-native-vector-icons/FontAwesome'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs,
} from 'native-base';

const arr = [
    {
        name: 'Vital',
        key: 0
    },
    {
        name: 'Vital',
        key: 1
    },
    {
        name: 'Vital',
        key: 2
    },
    {
        name: 'Vital',
        key: 3
    },
    {
        name: 'Vital',
        key: 4
    },
    {
        name: 'Vital',
        key: 5
    }

]

const BloodArray=[
    {
        date:'25 mar',
        bloodpre:'82/125',
        key:0
    },
    {
        date:'24 mar',
        bloodpre:'80/120',
        key:1
    },
    {
        date:'23 mar',
        bloodpre:'82/125',
        key:2
    },
    {
        date:'22 mar',
        bloodpre:'102/200',
        key:3
    },
    {
        date:'21 mar',
        bloodpre:'102/200',
        key:4
    },
]

export default class CaptureVManually extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action: false,
            captureModal: false
        };
    }

    ModalClose = () => {
        this.setState({
            captureModal: false
        })
    }

    render() {
        return (
            <Container style={{ backgroundColor: '#707373' }}>
                <View>
                    <View style={styles.headerView}>
                        <View style={{ justifyContent: 'flex-start', flexDirection: 'row', alignSelf: 'center' }}>
                            <Icon onPress={() => { this.state.captureModal ? this.setState({ captureModal: false }) : this.props.navigation.navigate('CaptureVitals') }} name='arrowleft' size={25} color='white' />
                            <Text style={styles.text}>Capture Vitals</Text>
                        </View>
                        <Thumbnail style={styles.img} large source={require('../Public/img/clinic-logo-png.png')} />
                    </View>
                    <Text style={styles.text1}>Capture Vitals Manually</Text>
                    <View style={styles.mapView}>
                        {arr.map((i, j) => {
                            return (
                                <TouchableWithoutFeedback onPress={() => { this.setState({ captureModal: true }) }}>
                                    <View>
                                        <View style={styles.view} />
                                        <Text style={{ alignSelf: 'center', color: 'white' }}>{i.name}</Text>
                                    </View>
                                </TouchableWithoutFeedback>
                            )
                        })}

                    </View>
                </View>
                <Modal
                    transparent={true}
                    visible={this.state.captureModal}
                    onRequestClose={() => {
                        this.setState({ captureModal: false })
                    }}
                >
                    <ScrollView>
                        <Card style={styles.card}>
                            <View style={{ borderWidth: 2, borderColor: 'grey', marginTop: 10, width: 70, alignSelf: 'center' }} />
                            <View style={styles.viewName}>
                                <Text style={styles.textmodal}>Date</Text>
                                <Text style={styles.textmodal1}>Vital name</Text>
                                <Text style={styles.textmodal} onPress={() => { this.setState({ captureModal: false }) }}>Close</Text>

                            </View>
                            <Text style={{ textAlign: 'right', marginRight: 20, fontSize: 14, marginTop: 30 }}>Share with Doctor</Text>
                            <Text style={{ textAlign: 'right', marginRight: 20, fontSize: 14, marginTop: 10 }}>How to capture</Text>
                            <View style={styles.viewName}>
                                <View style={{ borderWidth: 0.5, borderColor: 'grey', marginTop: 10, width: 150, alignSelf: 'center' }} />
                                <Text>save</Text>
                            </View>
                            <Text style={{ marginLeft: 90 }}>unit</Text>
                            <View style={styles.viewName}>
                                <Text style={{}}>vital trends</Text>
                                <Text style={{}}>Graph Filters</Text>
                            </View>
                            <Image
                                resizeMode='cover'
                                style={styles.imgb}
                                source={require('../Public/img/blood1.jpeg')}
                            />
                            {BloodArray.map((i,j)=>{
                                return(
                                    <View style={styles.viewName11}>
                                        <Text style={{color:i.key==1?'black':'grey',fontSize:15,alignSelf:'center', width:wp('20%')}}>{i.date}</Text>
                                        {i.key==1
                                        ?
                                        <View style={styles.viewcircle1}>
                                        <View style={[
                                            styles.viewcircle,
                                            { margin: 1 }
                                        ]} />
                                    </View>
                                    :
                                    <View style={styles.viewcircle} />
                                        }
                                        <Text style={{color:i.key==1?'black':'grey',fontSize:25,fontWeight:'bold',alignSelf:'center', width:wp('30%')}}>{i.bloodpre}</Text>
                                    </View>
                                )
                            })}
                         
                        </Card>
                    </ScrollView>

                </Modal>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    headerView: {
        justifyContent: 'space-between', flexDirection: 'row', width: wp('93%'), alignSelf: 'center', marginTop: 10
    },
    imgb: {
        width: wp('92%'),
        alignSelf: 'center',
        height: 200,
        marginTop: 5,
        marginBottom: 20
    },
    img: {
        height: 40, width: 40, borderRadius: 20, alignSelf: 'center', backgroundColor: 'white'
    },
    text: {
        marginLeft: 20, color: 'white', fontWeight: 'bold', fontSize: 18
    },
    text1: {
        alignSelf: 'center', textAlign: 'center', fontSize: 17, fontWeight: '900', color: 'white', marginTop: 30, marginBottom: 20
    },
    mapView: {
        justifyContent: 'flex-start', flexDirection: 'row', width: wp('90%'), flexWrap: 'wrap', alignSelf: 'center'
    },
    view: {
        height: 60, width: 60, borderRadius: 30, backgroundColor: 'white', alignSelf: 'center', margin: 10
    },
    card: {
        alignSelf: 'center',
        height: 'auto',
        width: wp('98%'),
        marginTop: 80,
        borderTopStartRadius:17,
        borderTopRightRadius:17
    },
    viewName: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignSelf: 'center',
        marginTop: 10,
        width: wp('85%')
    },
    viewName11: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: wp('60%'),
        marginLeft:30,
        marginBottom:10
    },
    textmodal: {
        fontSize: 13
    },
    textmodal1: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'grey'
    },
    viewcircle: {
        width: 10,
        height: 10,
        borderRadius: 5,
        backgroundColor: 'red',
        alignSelf:'center'
    },
    viewcircle1: {
        alignSelf: 'center', justifyContent: 'center', height: 14, width: 14, borderRadius: 7, borderWidth: 1, borderColor: 'red'
    }

});
