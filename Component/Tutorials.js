import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Icon, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';
export default class Tutorials extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Tutorials1: true,
            Tutorials2: false,
            Tutorials3: false,
            Tutorials4: false,
        };
    }


    render() {
        return (
            <Container>
                {this.state.Tutorials1 == true ?
                    <View style={styles.mainView}>
                        <Text style={styles.text1}>Skip</Text>

                        <Image
                            resizeMode='cover'
                            style={styles.img}
                            source={require('../Public/img/dow.png')}
                        />
                        <Text style={styles.title}>Title</Text>
                        <Text style={styles.subtitle}>Two lines here to describe the feature in a simple way</Text>

                        <View style={styles.view}>
                            <Text style={styles.leftText}> </Text>
                            <View style={styles.view1}>
                                <View style={styles.viewcircle1}>
                                    <View style={[
                                        styles.viewcircle,
                                        { margin: 1 }
                                    ]} />
                                </View>
                                <View style={styles.viewcircle} />
                                <View style={styles.viewcircle} />
                                <View style={styles.viewcircle} />

                            </View>
                            <Text style={styles.rightText}
                                onPress={() => {
                                    this.setState({
                                        Tutorials1: false,
                                        Tutorials2: true,
                                        Tutorials3: false,
                                        Tutorials4: false,
                                    })
                                }}
                            >Next</Text>


                        </View>
                    </View>
                    :
                    this.state.Tutorials2 == true ?
                        <View style={styles.mainView}>
                            <Text style={styles.text1}>Skip</Text>
                            <Image
                                resizeMode='cover'
                                style={styles.img}
                                source={require('../Public/img/docpat.jpeg')}
                            />
                            <Text style={styles.title}>Title</Text>
                            <Text style={styles.subtitle}>Two lines here to describe the feature in a simple way</Text>

                            <View style={styles.view}>
                                <Text style={styles.leftText}
                                    onPress={() => {
                                        this.setState({
                                            Tutorials1: true,
                                            Tutorials2: false,
                                            Tutorials3: false,
                                            Tutorials4: false,
                                        })
                                    }}
                                >Back</Text>
                                <View style={styles.view1}>

                                    <View style={styles.viewcircle} />
                                    <View style={styles.viewcircle1}>
                                        <View style={[
                                            styles.viewcircle,
                                            { margin: 1 }
                                        ]} />
                                    </View>
                                    <View style={styles.viewcircle} />
                                    <View style={styles.viewcircle} />

                                </View>
                                <Text style={styles.rightText}
                                    onPress={() => {
                                        this.setState({
                                            Tutorials1: false,
                                            Tutorials2: false,
                                            Tutorials3: true,
                                            Tutorials4: false,
                                        })
                                    }}
                                >Next</Text>


                            </View>
                        </View>
                        :
                        this.state.Tutorials3 == true ?
                            <View style={styles.mainView}>
                                <Text style={styles.text1}>Skip</Text>
                                <Image
                                    resizeMode='contain'
                                    style={styles.img}
                                    source={require('../Public/img/man.jpeg')}
                                />
                                <Text style={styles.title}>Title</Text>
                                <Text style={styles.subtitle}>Two lines here to describe the feature in a simple way</Text>

                                <View style={styles.view}>
                                    <Text style={styles.leftText}
                                        onPress={() => {
                                            this.setState({
                                                Tutorials1: false,
                                                Tutorials2: true,
                                                Tutorials3: false,
                                                Tutorials4: false,
                                            })
                                        }}
                                    >Back</Text>
                                    <View style={styles.view1}>

                                        <View style={styles.viewcircle} />
                                        <View style={styles.viewcircle} />
                                        <View style={styles.viewcircle1}>
                                            <View style={[
                                                styles.viewcircle,
                                                { margin: 1 }
                                            ]} />
                                        </View>
                                        <View style={styles.viewcircle} />

                                    </View>
                                    <Text style={styles.rightText}
                                        onPress={() => {
                                            this.setState({
                                                Tutorials1: false,
                                                Tutorials2: false,
                                                Tutorials3: false,
                                                Tutorials4: true,
                                            })
                                        }}
                                    >Next</Text>


                                </View>
                            </View>
                            :
                            <View style={styles.mainView}>
                                <Text style={styles.text1}>Skip</Text>
                                <Image
                                    resizeMode='cover'
                                    style={styles.img}
                                    source={require('../Public/img/dow.png')}
                                />
                                <Text style={styles.title}>Title</Text>
                                <Text style={styles.subtitle}>Two lines here to describe the feature in a simple way</Text>

                                <View style={styles.view}>
                                    <Text style={styles.leftText}
                                        onPress={() => {
                                            this.setState({
                                                Tutorials1: false,
                                                Tutorials2: false,
                                                Tutorials3: true,
                                                Tutorials4: false,
                                            })
                                        }}
                                    >Back</Text>
                                    <View style={styles.view1}>

                                        <View style={styles.viewcircle} />
                                        <View style={styles.viewcircle} />
                                        <View style={styles.viewcircle} />
                                        <View style={styles.viewcircle1}>
                                            <View style={[
                                                styles.viewcircle,
                                                { margin: 1 }
                                            ]} />
                                        </View>

                                    </View>
                                    <Text style={styles.rightText}
                                        onPress={() => { 
                                            this.setState({
                                                Tutorials1: true,
                                                Tutorials2: false,
                                                Tutorials3: false,
                                                Tutorials4: false,
                                            })
                                            this.props.navigation.navigate('Login') }}
                                    >Next</Text>


                                </View>
                            </View>
                }

            </Container>
        );
    }
}

const styles = StyleSheet.create({

    img: {
        height: 300, width: 300, alignSelf: 'center', marginTop: 30
    },
    mainView: {
        alignSelf: 'center',
        width: wp('97%'),
        height: hp('100%')
    },

    text: {
        position: 'absolute',
        bottom: 100
    },
    text1: {
        marginTop: 30,
        textAlign: 'right',
        marginRight: 20,
        fontSize: 13
    },
    title: {
        alignSelf: 'center',
        textAlign: 'center',
        color: '#38ced9',
        fontSize: 25,
        marginTop:10
    },
    subtitle: {
        alignSelf: 'center',
        textAlign: 'center',
        color: '#38ced9',
        fontSize: 13,
        width: wp('85%'),
        marginTop: 10
    },
    view: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: wp('85%'),
        alignSelf: 'center',
        position: 'absolute',
        bottom: 0,
        marginBottom: 50
    },
    view1: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        // width:wp('40%'),
        alignSelf: 'center'
    },
    viewcircle: {
        width: 6,
        height: 6,
        borderRadius: 3,
        backgroundColor: '#38ced9',
        margin: 5
    },
    leftText: {
        width: wp('25%'), textAlign: 'left', fontSize: 13
    },
    rightText: {
        width: wp('25%'), textAlign: 'right', fontSize: 13
    },
    viewcircle1: {
        alignSelf: 'center', justifyContent: 'center', height: 10, width: 10, borderRadius: 5, borderWidth: 1, borderColor: '#38ced9'
    }

});
