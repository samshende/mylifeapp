import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    ScrollView,
    TouchableWithoutFeedback
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign'
import IconUser from 'react-native-vector-icons/FontAwesome5'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';
export default class QRcode extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action: false
        };
    }


    render() {
        return (
            <Container style={{}}>
                <View style={{ backgroundColor: '#2580cf', width: wp('100%'), height:70 }}>
                        <View style={{ justifyContent: 'flex-start', flexDirection: 'row',marginTop:20}}>
                            <Icon onPress={() => { this.props.navigation.navigate('AuthorisePayment') }} style={{marginLeft:20,alignSelf:'center'}} name='arrowleft' size={25} color='white' />
                            <Text style={styles.text}>Authorise Payment</Text>
                    </View>
                </View>
                <ScrollView>
                    <View style={styles.headerView}>
                    <Thumbnail style={styles.img} large source={require('../../Public/img/doc3.jpeg')} />
                        <View style={{marginLeft:20,width:wp('65%')}}>
                            <Text style={{fontWeight:'bold',fontSize:17,}}>Dr.Rishabh Bhatia</Text>
                            <Text style={{fontSize:12}}>has Requested for a Payment for</Text>
                            <Text style={{marginTop:5,marginBottom:5,color:'grey',fontSize:25,fontWeight:'bold'}}>Rs.1,430</Text>
                            <Text style={{width:wp('45%'),color:'grey',fontSize:13}}>
                            If I want to apply I have to do more than
                            </Text>
                        </View>
                    </View>
                    
                            <Card style={{width:wp('80%'),alignSelf:'center',height:330,marginTop:10,backgroundColor:'#b6b5ba'}}>
                                <View style={{justifyContent:'center',flex:1}}>
                                    <Text style={{color:'white',alignSelf:'center',fontWeight:'bold'}}>Scan</Text>
                                </View>

                            </Card>
                <Button onPress={() => { this.props.navigation.navigate('Receipt') }} full style={styles.buttonReg}><Text>Authorise</Text></Button>
               
                </ScrollView>
                </Container>
        );
    }
}

const styles = StyleSheet.create({
    headerView: {
        justifyContent: 'flex-start', flexDirection: 'row', width: wp('93%'), alignSelf: 'center', marginTop: 20
    },
    img: {
        height: 90, width: 90, borderRadius: 45, alignSelf: 'center', backgroundColor: 'white'
    },
    text: {
        marginLeft: 20, color: 'white', fontWeight: 'bold', fontSize: 18
    },
    buttonReg: {
        width: wp('90%'),
        alignSelf: 'center',
        backgroundColor:'#2580cf',
        marginBottom:20,
        marginTop:20,
        borderRadius:10
    },
  

});
