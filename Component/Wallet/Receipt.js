import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    ScrollView,
    TouchableWithoutFeedback
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign'
import IconUser from 'react-native-vector-icons/FontAwesome5'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';
export default class Receipt extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action: false
        };
    }


    render() {
        return (
            <Container style={{}}>
                <View style={{ backgroundColor: '#2580cf', width: wp('100%'), height: 70 }}>
                    <View style={{ justifyContent: 'flex-start', flexDirection: 'row', marginTop: 20 }}>
                        <Icon onPress={() => { this.props.navigation.navigate('Integrationpage') }} style={{ marginLeft: 20, alignSelf: 'center' }} name='arrowleft' size={25} color='white' />
                        <Text style={styles.text}>Payment Done</Text>
                    </View>
                </View>
                <View style={{justifyContent:'center',flex:1}}>
                        <Text style={{fontSize:18,fontWeight:'bold',marginLeft:40}}>Vendor Payment Page</Text>
                        <Text style={{fontSize:18,fontWeight:'bold',marginLeft:40,marginTop:30}} onPress={() => { this.props.navigation.navigate('CliniQWallet') }}>To wallet</Text>

                    </View>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    headerView: {
        justifyContent: 'flex-start', flexDirection: 'row', width: wp('93%'), alignSelf: 'center', marginTop: 20
    },
    img: {
        height: 90, width: 90, borderRadius: 45, alignSelf: 'center', backgroundColor: 'white'
    },
    text: {
        marginLeft: 20, color: 'white', fontWeight: 'bold', fontSize: 18
    },


});
