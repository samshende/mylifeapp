
import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    ScrollView,
    TouchableWithoutFeedback
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather'
import IconUser from 'react-native-vector-icons/FontAwesome5'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';
const arr = [1, 2]
export default class CliniQWallet extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action: false
        };
    }


    render() {
        return (
            <Container style={{}}>
                <View style={{ backgroundColor: '#2580cf', width: wp('100%'), height: 'auto' }}>
                    <View style={styles.headerView}>
                        <View style={{ justifyContent: 'flex-start', flexDirection: 'row', alignSelf: 'center' }}>
                            <Icon name='menu' size={25} color='white' />
                            <Text style={styles.text}>Your CliniQ Wallet</Text>
                        </View>
                        <Thumbnail style={styles.img} large source={require('../../Public/img/doc3.jpeg')} />
                    </View>
                    <View style={styles.headerView1}>
                        <View style={{ marginLeft: 10, marginTop: -25 }}>
                            <Text style={{ color: 'white', fontSize: 10 }}>Current Balance</Text>
                            <Text style={{ color: 'white', fontSize: 30, fontWeight: 'bold' }}>Rs.12,345</Text>
                        </View>
                        <TouchableWithoutFeedback onPress={() => { this.props.navigation.navigate('Addmoney') }}>
                            <View style={{ alignItems: 'center', alignContent: 'center' }}>
                                <View style={{ height: 24, width: 24, borderRadius: 12, backgroundColor: 'white' }} />
                                <Text style={{ color: 'white', fontSize: 11, marginTop: 2, marginBottom: 20, fontWeight: 'bold' }}>Add Money</Text>
                            </View>
                        </TouchableWithoutFeedback>

                    </View>
                </View>
                <ScrollView>
                    <View style={styles.headerView}>
                        <Text>Payments</Text>
                        <Text>Make A new Payment</Text>
                    </View>
                    <Card style={styles.card}>
                        <View>
                            <Text style={{ marginLeft: 10, marginTop: 10, fontWeight: 'bold' }}>Make A new Payment</Text>
                            <View style={styles.cardView}>
                                <Thumbnail style={styles.img} large source={require('../../Public/img/doc3.jpeg')} />
                                <View
                                    style={{
                                        borderBottomColor: 'grey',
                                        borderBottomWidth: 1,
                                        width: wp('15%'),
                                        alignSelf: 'center'
                                    }}
                                />
                                <Text style={{ alignSelf: 'center', color: 'grey', fontSize: 20, fontWeight: 'bold' }}>Rs.1,430</Text>
                                <View
                                    style={{
                                        borderBottomColor: 'grey',
                                        borderBottomWidth: 1,
                                        width: wp('15%'),
                                        alignSelf: 'center'
                                    }}
                                />
                                <TouchableWithoutFeedback onPress={() => { this.props.navigation.navigate('AuthorisePayment') }}>
                                    <View style={{ backgroundColor: '#2580cf', height: 70, width: 70, borderRadius: 10, justifyContent: 'center' }}>
                                        <Text style={{ alignSelf: 'center', color: 'white', fontSize: 10 }}>Authorise</Text>
                                    </View>
                                </TouchableWithoutFeedback>

                            </View>
                        </View>
                    </Card>
                    <View style={styles.headerView}>
                        <Text>Past Transactions</Text>
                        <Text>Filter</Text>
                    </View>
                    {arr.map((i, j) => {
                        return (

                            <Card style={styles.card1}>
                                <View style={styles.cardView}>
                                    <View style={styles.cardView1}>
                                        <View style={{ width: wp('28%'), alignItems: 'center', alignContent: 'center', marginLeft: 5 }}>
                                            <Text style={{ color: 'grey', textAlign: 'center', fontSize: 12, marginTop: 10 }}>Date</Text>
                                            <Text style={{ color: 'grey', textAlign: 'center', fontSize: 20, fontWeight: 'bold' }}>Rs.1,430</Text>
                                            <Text style={{ color: 'grey', textAlign: 'center', fontSize: 10 }}>If I want to apply I have to do more than</Text>
                                        </View>
                                        <View style={{ width: wp('2%') }} />
                                        <View style={{
                                            borderStyle: 'solid',
                                            height: 110,
                                            borderLeftWidth: 0.3, marginTop: 15, borderColor: 'grey'
                                        }} />
                                        <View style={{ width: wp('2%') }} />
                                    </View>
                                    <View style={{ flexDirection: 'column', alignSelf: 'center' }}>
                                        <View style={styles.cardView2}>
                                            <View style={{ alignSelf: 'center', width: wp('19%') }}>
                                                <Thumbnail style={styles.img} large source={require('../../Public/img/doc3.jpeg')} />
                                                <Text style={{ fontSize: 13, color: 'grey', textAlign: 'center' }}>Dr.Uday Patil</Text>
                                            </View>
                                            <View
                                                style={{
                                                    borderBottomColor: 'grey',
                                                    borderBottomWidth: 1,
                                                    width: wp('15%'),
                                                    alignSelf: 'center',
                                                    marginTop: -25
                                                }}
                                            />
                                            <View style={{ alignSelf: 'center', width: wp('19%') }}>
                                                <Thumbnail style={styles.img} large source={require('../../Public/img/doc4.jpeg')} />
                                                <Text style={{ fontSize: 13, color: 'grey', textAlign: 'center' }}>Dr.umesh Patil</Text>
                                            </View>
                                        </View>
                                        <View>
                                            <Text style={{ textAlign: 'center', marginTop: 15, color: '#2580cf' }}>View Invoice</Text>
                                        </View>
                                    </View>
                                </View>
                            </Card>

                        )
                    })}
                    <Text style={{ alignSelf: 'center', fontSize: 18, fontWeight: 'bold', color: '#2580cf', marginTop: 20, marginBottom: 20, fontStyle: 'italic' }}>
                        How Will The User get his invoice?
                    </Text>
                </ScrollView>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    headerView: {
        justifyContent: 'space-between', flexDirection: 'row', width: wp('93%'), alignSelf: 'center', marginTop: 10
    },
    img: {
        height: 40, width: 40, borderRadius: 20, alignSelf: 'center', backgroundColor: 'white'
    },
    text: {
        marginLeft: 20, color: 'white', fontWeight: 'bold', fontSize: 18
    },
    headerView1: {
        justifyContent: 'space-between', flexDirection: 'row', width: wp('93%'), alignSelf: 'center', marginTop: 40
    },
    card: {
        alignSelf: 'center',
        width: wp('92%'),
        height: 120,
        marginTop: 10
    },
    card1: {
        alignSelf: 'center',
        width: wp('92%'),
        height: 140,
        marginBottom: 10
    },
    cardView: {
        justifyContent: 'space-between', flexDirection: 'row', width: wp('88%'), alignSelf: 'center'
    },
    cardView1: {
        justifyContent: 'space-between', flexDirection: 'row', width: wp('35%'),
    },
    cardView2: {
        justifyContent: 'space-between', flexDirection: 'row', width: wp('53%'),
    },

});
