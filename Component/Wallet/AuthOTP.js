import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    ScrollView,
    TouchableWithoutFeedback
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign'
import IconUser from 'react-native-vector-icons/FontAwesome5'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';
import OTPTextView from 'react-native-otp-textinput';
export default class AuthOTP extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action: false,
            otpInput: '',

        };
    }


    render() {
        return (
            <Container style={{}}>
                <View style={{ backgroundColor: '#2580cf', width: wp('100%'), height:70 }}>
                        <View style={{ justifyContent: 'flex-start', flexDirection: 'row',marginTop:20}}>
                            <Icon onPress={() => { this.props.navigation.navigate('AuthorisePayment') }} style={{marginLeft:20,alignSelf:'center'}} name='arrowleft' size={25} color='white' />
                            <Text style={styles.text}>Authorise Payment</Text>
                    </View>
                </View>
                <ScrollView>
                    <View style={styles.headerView}>
                    <Thumbnail style={styles.img} large source={require('../../Public/img/doc3.jpeg')} />
                        <View style={{marginLeft:20,width:wp('65%')}}>
                            <Text style={{fontWeight:'bold',fontSize:17,}}>Dr.Rishabh Bhatia</Text>
                            <Text style={{fontSize:12}}>has Requested for a Payment for</Text>
                            <Text style={{marginTop:5,marginBottom:5,color:'grey',fontSize:25,fontWeight:'bold'}}>Rs.1,430</Text>
                            <Text style={{width:wp('45%'),color:'grey',fontSize:13}}>
                            If I want to apply I have to do more than
                            </Text>
                        </View>
                    </View>
                    <Text style={{marginTop:20,marginLeft:15,fontSize:13}}>An OTP has been sent to your registered number</Text>
                    <Text style={{color:'grey',marginLeft:15,fontSize:12}}>(99XXXXXX80)</Text>
                    <OTPTextView
          ref={(e) => (this.input1 = e)}
          tintColor={'#DCDCDC'}
          containerStyle={styles.textInputContainer}
          handleTextChange={(text) => this.setState({otpInput: text})}
          inputCount={6}
          keyboardType="numeric"
        />
                    <Text style={{color:'#67547d',marginLeft:15,fontSize:12}}>Resend OTP</Text>
                <Button onPress={() => { this.props.navigation.navigate('Receipt') }} full style={styles.buttonReg}><Text>Verify By OTP</Text></Button>
               
                </ScrollView>
                </Container>
        );
    }
}

const styles = StyleSheet.create({
    headerView: {
        justifyContent: 'flex-start', flexDirection: 'row', width: wp('93%'), alignSelf: 'center', marginTop: 20
    },
    img: {
        height: 90, width: 90, borderRadius: 45, alignSelf: 'center', backgroundColor: 'white'
    },
    text: {
        marginLeft: 20, color: 'white', fontWeight: 'bold', fontSize: 18
    },
    buttonReg: {
        width: wp('90%'),
        alignSelf: 'center',
        backgroundColor:'#2580cf',
        marginBottom:20,
        marginTop:20,
        borderRadius:10
    },
    textInputContainer: {
        marginBottom:20,
      },

});
