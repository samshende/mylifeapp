import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    ScrollView,
    TouchableWithoutFeedback
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign'
import IconUser from 'react-native-vector-icons/FontAwesome5'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';
const arr = [1, 2]
export default class Addmoney extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action: false,
            arr:[
                {
                    id:0,
                    amount:1000,
                    status:false
                },
                {
                    id:1,
                    amount:1000,
                    status:false
                },  {
                    id:2,
                    amount:1000,
                    status:false
                },  {
                    id:3,
                    amount:1000,
                    status:false
                },
            ]
        };
    }

    selectAmount=(i)=>{
        let changeStatus = ''
                    changeStatus = this.state.arr.find((cb) => cb.id === i.id);
                    changeStatus.status =!changeStatus.status
                    console.log("c", changeStatus)
                    const arr = Object.assign([], this.state.arr, changeStatus);
                    this.setState({
                       arr:arr
                    })
    }

    render() {
        return (
            <Container style={{}}>
                <View style={{ backgroundColor: '#2580cf', width: wp('100%'), height: 'auto' }}>
                    <View style={styles.headerView}>
                        <View style={{ justifyContent: 'flex-start', flexDirection: 'row', alignSelf: 'center' }}>
                            <Icon name='arrowleft' size={25} color='white' />
                            <Text style={styles.text}>Your CliniQ Wallet</Text>
                        </View>
                        <Thumbnail style={styles.img} large source={require('../../Public/img/doc3.jpeg')} />
                    </View>
                    <Text style={{ color: 'white', fontSize: 10, marginLeft: 30, marginTop: 10 }}>Current Balance</Text>
                    <Text style={{ color: 'white', fontSize: 30, fontWeight: 'bold', marginLeft: 30, marginBottom: 30 }}>Rs.12,345</Text>
                </View>
                <ScrollView>
                    <Text style={{ marginTop: 20, fontSize: 15, marginLeft: 15 }}>
                        Enter  Amount to recharge
                    </Text>
                    <Item style={{ width: wp('70%'), marginLeft: 15 }}>
                        <Input placeholder="" disabled={true} />
                    </Item>
                    <Text style={{ marginTop: 20, fontSize: 15, marginLeft: 15, marginBottom: 15 }}>Quick Amounts</Text>

                    <View style={styles.CardView}>
                        {this.state.arr.map((i, j) => {
                            return (
                                <TouchableWithoutFeedback onPress={()=>{this.selectAmount(i)}}>
                                <Card style={{ height: 50, width: 70, backgroundColor:i.status?'grey': 'white' }}>
                                        <Text style={{ textAlign: 'center', color:i.status?'white': 'grey',alignSelf:'center',fontSize:15,marginTop:12 }}>Rs.{i.amount}</Text>
                                </Card>
                                </TouchableWithoutFeedback>
                            )
                        })}

                    </View>
                    <Text style={{ marginTop: 20, fontSize: 15, marginLeft: 15, marginBottom: 15 }}>Pay Using</Text>
                    <Card style={styles.card}>
                        <Text style={{marginTop:30,marginLeft:20,fontWeight:'bold',fontSize:25,color:'grey'}}>
                            Payment Options
                        </Text>

                    </Card>

                    <Button onPress={() => { this.props.navigation.navigate('Integrationpage') }} full style={styles.buttonReg}><Text>Proceed to pay</Text></Button>
                </ScrollView>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    headerView: {
        justifyContent: 'space-between', flexDirection: 'row', width: wp('93%'), alignSelf: 'center', marginTop: 20
    },
    img: {
        height: 40, width: 40, borderRadius: 20, alignSelf: 'center', backgroundColor: 'white'
    },
    text: {
        marginLeft: 20, color: 'white', fontWeight: 'bold', fontSize: 18
    },
    CardView: {
        justifyContent: 'space-between', flexDirection: 'row', width: wp('93%'), alignSelf: 'center', marginTop: 5
    },
card:{
    width:wp('93%'),alignSelf:'center',height:100
},
buttonReg: {
    width: wp('70%'),
    alignSelf: 'center',
    backgroundColor:'grey',
    marginBottom:15,marginTop:15
},

});
