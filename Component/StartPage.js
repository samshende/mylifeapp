import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    Text,
    View
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default class HomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount() {
        this.timeoutHandle = setTimeout(() => {
            this.props.navigation.navigate('Tutorials') // what to push here?
        }, 5000);
    }
    componentWillUnmount() {
        clearTimeout(this.timeoutHandle);
    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center"
                }}>
                <Image
                    resizeMode='center'
                    style={styles.img}
                    source={require('../Public/img/clinic-logo-png-1.png')}
                />
            </View>

        );
    }
}

const styles = StyleSheet.create({

    img: {
        height: 300, width: 300,
    },

});
