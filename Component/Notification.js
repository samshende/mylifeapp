import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather'
import Icon1 from 'react-native-vector-icons/FontAwesome'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';


Notification = [
    {
        name: 'Today',
        not: [1, 2, 3]
    },
    {
        name: 'Yesterday',
        not: [1, 2, 3]
    }
]

export default class Notifications extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action: false
        };
    }
    // ViewInsurance
    // AddInsurance

    render() {
        return (
            <Container>
                <Header style={styles.header}>
                    <Left>
                        <Icon name='menu' size={30} />
                    </Left>
                    <Body>
                        <Text style={{ textAlign: 'center' }}>My  Notifications</Text>
                    </Body>
                    <Right>
                        <Icon1 style={styles.icon} color='#7a7979' name='user-circle' size={30} />
                    </Right>
                </Header>
                <ScrollView>
                    {Notification.map((i, j) => {
                        return (
                            <>
                                <Text style={{ marginTop:20, marginLeft: 20, marginBottom: 5,fontSize:14,color:'grey' }}>{i.name}</Text>
                                {i.not.map((a, b) => {
                                    return (

                                        <Card style={styles.card}>
                                        </Card>

                                    )
                                })}
                            </>
                        )
                    })}
                </ScrollView>
            </Container>
        );
    }
}

const styles = StyleSheet.create({

    header: {
        backgroundColor: 'white'
    },

    icon: {
        marginLeft: 15
    },
    card: {
        alignSelf: 'center', width: wp('90%'), height: 80
    },

});
