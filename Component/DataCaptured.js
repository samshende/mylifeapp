
import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign'
import Icon1 from 'react-native-vector-icons/FontAwesome'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';

const arr = [1, 4]

export default class DataCaptured extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action: false
        };
    }


    render() {
        return (
            <Container style={{}}>
                <View style={{ backgroundColor: '#707373', width: wp('100%'), height: 70 }}>
                    <View style={styles.headerView}>
                        <View style={{ justifyContent: 'flex-start', flexDirection: 'row', alignSelf: 'center' }}>
                            <Icon onPress={() => { this.props.navigation.navigate('TestCompleted') }} name='arrowleft' size={25} color='white' />
                            <Text style={styles.text}>Capture Vitals</Text>
                        </View>
                        <Thumbnail style={styles.img} large source={require('../Public/img/clinic-logo-png.png')} />
                    </View>
                </View>
                <ScrollView>
                    <Text style={{position:'absolute',right:20,top:20,fontSize:12,color:'#7296ba'}}>Download</Text>

                <Card style={styles.cardlarge}>
                    <View style={styles.cardViewlarge}>
                        <Text style={{fontSize:13}}>Vital Recorded and Saved Acknowledgment</Text>
                    </View>

                </Card>
                <Button onPress={() => { this.props.navigation.navigate('MyDoctor') }} full style={styles.buttonReg}><Text>Send to Doctor</Text></Button>
                <Button onPress={() => { this.props.navigation.navigate('CaptureVDevice') }} full style={styles.buttonReg}><Text>Take Another Test</Text></Button>
                <Button onPress={() => { this.props.navigation.navigate('HomeScreen') }} full style={styles.buttonReg}><Text>To Dashboard</Text></Button>
                </ScrollView>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    headerView: {
        justifyContent: 'space-between', flexDirection: 'row', width: wp('93%'), alignSelf: 'center', marginTop: 10
    },
    img: {
        height: 40, width: 40, borderRadius: 20, alignSelf: 'center', backgroundColor: 'white'
    },
    text: {
        marginLeft: 20, color: 'white', fontWeight: 'bold', fontSize: 18
    },
    scrollview: {
        justifyContent: 'space-between', flexDirection: 'row', width: wp('90%'), alignSelf: 'center', marginTop: 40, marginBottom: 20
    },

    cardlarge:{
        width:wp('92%'),
        height:400,
        alignSelf:'center',
        borderColor:'black',marginBottom:30,marginTop:60
    },
    cardViewlarge:{
        justifyContent:'center',flex:1,alignItems:'center'
    },
    buttonReg: {
        width: wp('90%'),
        alignSelf: 'center',
        backgroundColor:'#414240',
        marginBottom:15
    },
    buttonView:{
        alignSelf:'center',width:wp('95%'),justifyContent:'space-between',flexDirection:'row'
    }
});
