import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    TouchableWithoutFeedback
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather'
import Icon1 from 'react-native-vector-icons/FontAwesome'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';



export default class CaptureVitals extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action:false
        };
    }


    render() {
        return (
            <Container>
                <Header style={styles.header}>
                    <Left>
                        <Icon name='menu' size={30} />
                    </Left>
                    <Body>
                        <Text style={{textAlign:'center'}}>My Vitals</Text>
                    </Body>
                    <Right>
                        <Icon1 onPress={() => { this.props.navigation.navigate('Notifications') }}  style={styles.icon} color='#7a7979' name='bell' size={30} />
                        <Icon1 style={styles.icon} color='#7a7979' name='user-circle' size={30} />
                    </Right>
                </Header>
                <View style={{marginTop:30}}>
                <TouchableWithoutFeedback onPress={() => { this.props.navigation.navigate('CaptureVManually') }}  >
                    <Card style={styles.card}>
                        <View style={styles.cardView}>
                            <Text style={styles.text}>Manually</Text>
                        </View>
                    </Card>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback onPress={() => { this.props.navigation.navigate('CaptureVDevice') }}  >
                    <Card style={styles.card}>
                        <View style={styles.cardView}>
                            <Text style={styles.text}>Through Device</Text>
                        </View>
                    </Card>
                    </TouchableWithoutFeedback>
                </View>
                   </Container>
        );
    }
}

const styles = StyleSheet.create({

   
    header: {
        backgroundColor: 'white'
    },
   
    icon: {
        marginLeft: 15
    },
    card:{
        width:wp('85%'),alignSelf:'center',height:hp('20%'),borderRadius:15,marginTop:20
    },
    cardView:{
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    },
    text:{
        color:'#2e2d2d',fontSize:18,fontWeight:'900'
    }
  
});
