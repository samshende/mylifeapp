import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    Modal,
    ScrollView,
    TouchableWithoutFeedback
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather'
import Icon1 from 'react-native-vector-icons/AntDesign'
import Icon2 from 'react-native-vector-icons/AntDesign'
import DrawerComp from './Drawer'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs, ScrollableTab
} from 'native-base';
import MyAppointmentsTabs from './MyAppointmentsTabs'
const arr = [1, 2, 3]

export default class MainTab extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }


    render() {
        console.log("props", this.props)
        return (
            <Container>
                <View style={{ backgroundColor: '#2a85c7', width: wp('100%'), height: 70 }}>
                    <View style={styles.headerView}>
                        <View style={{ justifyContent: 'flex-start', flexDirection: 'row', alignSelf: 'center' }}>
                            <Thumbnail style={styles.img1} large source={require('../Public/img/clinic-logo-png.png')} />
                            <Text style={styles.text}>Shweta Vani</Text>
                        </View>
                    </View>
                </View>
                <Tabs renderTabBar={() => <ScrollableTab />} tabBarUnderlineStyle={{ borderBottomWidth: 3, borderBottomColor: 'grey' }}>
                    <Tab
                        tabStyle={{ backgroundColor: 'white' }}
                        activeTabStyle={{ backgroundColor: 'white' }}
                        textStyle={{ textAlign: 'center', color: 'grey' }}
                        activeTextStyle={{ color: 'black', textAlign: 'center' }}
                        heading="My Appointments">
                        <MyAppointmentsTabs />
                    </Tab>
                    <Tab
                        tabStyle={{ backgroundColor: 'white' }}
                        activeTabStyle={{ backgroundColor: 'white' }}
                        textStyle={{ textAlign: 'center', color: 'grey' }}
                        activeTextStyle={{ color: 'black', textAlign: 'center' }}
                        heading="My Vitals">
                        <View style={styles.viewTop}>
                            <Text style={{ color: 'grey', fontSize: 15 }}>Filter</Text>
                            <Text style={{ color: 'grey', marginRight: 10, fontSize: 15 }}>Add New</Text>
                        </View>

                        <Card style={styles.card11}>
                            <View style={styles.cardview}>
                                <Text style={{ alignSelf: 'center', color: 'grey' }}>Date</Text>
                                <View style={{ alignSelf: 'center', marginLeft: 15, fontSize: 13 }}>
                                    <Text style={{ color: 'grey', fontSize: 13 }}>Vital Report for</Text>
                                    <Text style={{ color: 'grey', fontSize: 13 }}>Blood presure</Text>
                                    <Text style={{ color: 'grey', fontSize: 13 }}>sugar</Text>
                                    <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                                        {arr.map((i, j) => {
                                            return (
                                                <View style={{ height: 20, width: 20, borderRadius: 10, borderWidth: 1, borderColor: 'grey', alignSelf: 'center', margin: 10, marginLeft: 0, marginTop: 5 }} />
                                            )
                                        })}
                                    </View>
                                </View>
                            </View>

                        </Card>
                    </Tab>
                    <Tab
                        tabStyle={{ backgroundColor: 'white' }}
                        activeTabStyle={{ backgroundColor: 'white' }}
                        textStyle={{ textAlign: 'center', color: 'grey' }}
                        activeTextStyle={{ color: 'black', textAlign: 'center' }}
                        heading="My Doctors">
                        <View style={styles.viewTop1}>
                            <View style={{ width: wp('19%') }}>
                                <Image
                                    resizeMode='contain'
                                    style={styles.img}
                                    source={require('../Public/img/doc3.jpeg')}
                                />
                            </View>
                            <Card style={styles.card}>
                                <View style={{ position: 'absolute', right: 0, top: 0, backgroundColor: '#19cbe3', height: 30, width: 30, }}>
                                    <Icon2 style={{ alignSelf: 'center', marginTop: 4 }} name='infocirlceo' size={20} color='white' />
                                </View>
                                <View style={{ margin: 10 }}>
                                    <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#5e5d5d' }}>Dr. Uday Shetty</Text>
                                    <Text style={{ fontSize: 11, color: 'grey' }}>General Medicine</Text>
                                    <Text style={{ fontWeight: 'bold', fontSize: 13, color: '#5e5d5d' }}>Qualification</Text>
                                    <Text style={{ fontSize: 12, color: 'grey' }}>MBBS,MS</Text>
                                    <View style={{ justifyContent: 'space-between', flexDirection: 'row', width: wp('55%'), marginLeft: 20, marginTop: 15 }}>
                                        <Text style={{ color: '#67547d', fontSize: 13 }}>Contact</Text>
                                        <Text style={{ color: '#538a53', fontSize: 13 }}>Book Appointment</Text>
                                    </View>
                                </View>
                            </Card>
                        </View>
                    </Tab>
                    <Tab
                        tabStyle={{ backgroundColor: 'white' }}
                        activeTabStyle={{ backgroundColor: 'white' }}
                        textStyle={{ textAlign: 'center', color: 'grey' }}
                        activeTextStyle={{ color: 'black', textAlign: 'center' }}
                        heading="My Reports">
                        <View style={styles.viewTop}>
                            <Text style={{ color: 'grey', fontSize: 15 }}>Filter</Text>
                            <Text style={{ color: 'grey', marginRight: 10, fontSize: 15 }}>Add New</Text>
                        </View>

                        <Card style={styles.card12}>
                            <View style={styles.cardview}>
                                <Text style={{ alignSelf: 'center', color: 'grey' }}>Date</Text>
                                <View style={{ alignSelf: 'center', marginLeft: 15, fontSize: 13 }}>
                                    <Text style={{ color: 'grey', fontSize: 13 }}>Report for Name</Text>
                                    <Text style={{ color: 'grey', fontSize: 13, marginTop: 15 }}>Report Type</Text>
                                </View>
                            </View>
                            <Text style={{ color: 'grey', fontSize: 13, position: 'absolute', right: 10, bottom: 10 }}>By Doctor</Text>

                        </Card>
                    </Tab>
                    <Tab
                        tabStyle={{ backgroundColor: 'white' }}
                        activeTabStyle={{ backgroundColor: 'white' }}
                        textStyle={{ textAlign: 'center', color: 'grey' }}
                        activeTextStyle={{ color: 'black', textAlign: 'center' }}
                        heading="My Insurance">
                        <View style={styles.viewTop}>
                            <Text style={{ color: 'grey', fontSize: 15 }}>Filter</Text>
                            <Text style={{ color: 'grey', marginRight: 10, fontSize: 15 }}>Add New</Text>
                        </View>

                        <Card style={styles.card12}>
                            <View style={{ marginTop: 15, marginLeft: 25, fontSize: 15 }}>
                                <Text style={{ color: 'grey' }}>Insurance Name</Text>
                                <Text style={{ color: 'grey' }}>Date</Text>
                            </View>

                        </Card>
                    </Tab>
                    <Tab
                        tabStyle={{ backgroundColor: 'white' }}
                        activeTabStyle={{ backgroundColor: 'white' }}
                        textStyle={{ textAlign: 'center', color: 'grey' }}
                        activeTextStyle={{ color: 'black', textAlign: 'center' }}
                        heading="My CliniQ Wallet">
                            <Text style={{textAlign:'right', color: 'grey', marginRight: 15,marginTop:15, fontSize: 15 }}>Add Money</Text>
                        <Text style={{ alignSelf:'center', fontSize: 10, marginTop: 10,color:'grey' }}>Current Balance</Text>
                        <Text style={{ alignSelf:'center', fontSize: 35, fontWeight: 'bold', marginBottom: 20,color:'grey' }}>Rs.12,345</Text>
                        <Text style={{ marginLeft:20,color: 'grey' }}>Date</Text>


                        <Card style={styles.card12}>
                            <View style={styles.cardview}>
                                <Text style={{ alignSelf: 'center', color: 'grey' }}>Amount</Text>
                                <View style={{ alignSelf: 'center', marginLeft: 15, fontSize: 13 }}>
                                    <Text style={{ color: 'grey', fontSize: 13 }}>Transaction by whom</Text>
                                    <Text style={{ color: 'grey', fontSize: 13, }}>For what reason</Text>
                                    <Text style={{ color: 'grey', fontSize: 13, }}>By which Doctor</Text>

                                </View>
                            </View>
                        </Card>
                    </Tab>
                </Tabs>
                <Footer style={{ backgroundColor: 'white', height: 70 }}>
                    <FooterTab style={{ backgroundColor: 'white', margin: 5 }}>
                        <TouchableWithoutFeedback>
                            <View>
                                <View style={styles.viewcircle} />
                                <Text style={styles.textfooter}>Dashboard</Text>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback>
                            <View>
                                <View style={styles.viewcircle} />
                                <Text style={styles.textfooter} >Appointments</Text>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback>
                            <View>
                                <View style={styles.viewcircle} />
                                <Text style={styles.textfooter} >Vitals</Text>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback>
                            <View>
                                <View style={styles.viewcircle} />
                                <Text style={styles.textfooter} >Documents</Text>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback>
                            <View>
                                <View style={styles.viewcircle} />
                                <Text style={styles.textfooter} >Account</Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    headerView: {
        justifyContent: 'space-between', flexDirection: 'row', width: wp('93%'), alignSelf: 'center', marginTop: 10
    },
    img1: {
        height: 40, width: 40, borderRadius: 20, alignSelf: 'center', backgroundColor: 'white'
    },
    text: {
        marginLeft: 20, color: 'white', fontSize: 18, alignSelf: 'center'
    },
    textfooter: {
        textAlign: 'center',
        fontSize: 12,
        color: 'grey'
    },
    viewcircle: {
        alignSelf: 'center',
        height: 30,
        width: 30,
        borderRadius: 15,
        backgroundColor: '#a38483'
    },
    card: {
        width: wp('70%'),
        height: 130,
        borderRadius: 6
    },
    viewTop1: {
        alignSelf: 'center',
        width: wp('90%'),
        justifyContent: 'flex-start',
        flexDirection: 'row',
        marginTop: 10
    },
    img: {
        height: 100, width: wp('20%'), marginTop: 7
    },
    viewTop: {
        width: wp('50%'),
        justifyContent: 'space-between',
        flexDirection: 'row', marginTop: 20, marginBottom: 20,
        alignSelf: 'flex-end'
    },
    card11: {
        alignSelf: 'center',
        width: wp('90%'),
        height: 120,
        borderRadius: 15
    },
    card12: {
        alignSelf: 'center',
        width: wp('90%'),
        height: 100,
        borderRadius: 15
    },
    cardview: {
        justifyContent: 'flex-start',
        flexDirection: 'row',
        margin: 20
    }
});
