import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Icon, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';
export default class SendOtp extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }


    render() {
        return (
            <Container>
                <View style={styles.mainView}>
                    <Image
                        resizeMode='center'
                        style={styles.img}
                        source={require('../Public/img/clinic-logo-png-1.png')}
                    />
                    <Item style={styles.lable} floatingLabel>
                        <Label >Enter OTP</Label>
                        <Input />
                    </Item>

                    <Button onPress={() => { this.props.navigation.navigate('HomeScreen') }} full style={styles.button}><Text>Login</Text></Button>
                    <Text style={styles.text1}>Resend</Text>

                </View>
            </Container>
        );
    }
}

const styles = StyleSheet.create({

    img: {
        height: 150, width: 150,
    },
    mainView: {
        flex: 1,
        alignItems: "center",
        alignContent: 'center',
        alignSelf: 'center',
        width: wp('95%')
    },
    lable: {
        width: wp('85%')
    },
    button: {
        width: wp('90%'),
        borderRadius: 7,
        alignSelf: 'center',
        marginTop: 30,
        backgroundColor:'#0f89f5',
    },
    text1: {
        marginTop: 20,
        alignSelf:'flex-start',
        marginLeft:20
    }

});
