

import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign'
import Icon1 from 'react-native-vector-icons/FontAwesome'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';

const arr=[
    {
        Key:0,
        name:"Report name"
    },
    {
        Key:1,
        name:"Report Type"
    },
    {
        Key:2,
        name:"Report Date"
    },
    {
        Key:3,
        name:"Consulting Doctor"
    },
    {
        Key:4,
        name:"Doctor Contact details"
    },

]

export default class AddInsurance extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action:false
        };
    }


    render() {
        return (
            <Container style={{backgroundColor:'white'}}>
                    <View style={{backgroundColor:'#707373',width:wp('100%'),height:70}}>
                    <View style={styles.headerView}>
                            <View style={{justifyContent:'flex-start',flexDirection:'row',alignSelf:'center'}}>
                            <Icon onPress={() => { this.props.navigation.navigate('MyInsurance') }} name='arrowleft' size={25} color='white' />
                            <Text style={styles.text}>Add an Insurance</Text>
                        </View>
                        <Thumbnail style={styles.img} large  source={require('../Public/img/clinic-logo-png.png')} />
                    </View>
                    </View>
                    <ScrollView>
                        <View style={styles.mainView}>
                          
                                    <Text style={{fontSize:18,fontWeight:'bold',color:'#707373',marginBottom:13}}>Search insurance</Text>
                                    <Text style={{fontSize:18,fontWeight:'bold',color:'#707373',marginBottom:13}}>Enter Insurance ID</Text>

                              
                        </View>
                    <Button onPress={() => { this.props.navigation.navigate('ViewInsurance') }} full style={styles.buttonReg}><Text>Upload</Text></Button>

                    </ScrollView>
              </Container>
        );
    }
}

const styles = StyleSheet.create({
    headerView:{
        justifyContent:'space-between',flexDirection:'row',width:wp('93%'),alignSelf:'center',marginTop:10
    },
    img:{
        height:40,width:40,borderRadius:20,alignSelf:'center',backgroundColor:'white'
    },
    text:{
        marginLeft:20,color:'white',fontWeight:'bold',fontSize:18
    },
    buttonReg: {
        width: wp('80%'),
        alignSelf: 'center',
        backgroundColor:'#707373',
        marginBottom:15
    },
    mainView:{
        margin:20,marginTop:50,marginBottom:200,marginLeft:40
    }
});
