
import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign'
import Icon1 from 'react-native-vector-icons/FontAwesome'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';

const arr = [1, 4]

export default class PairingDeviceS extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action: false
        };
    }


    render() {
        return (
            <Container style={{}}>
                <View style={{ backgroundColor: '#707373', width: wp('100%'), height: 70 }}>
                    <View style={styles.headerView}>
                        <View style={{ justifyContent: 'flex-start', flexDirection: 'row', alignSelf: 'center' }}>
                            <Icon onPress={() => { this.props.navigation.navigate('PairingDevice') }} name='arrowleft' size={25} color='white' />
                            <Text style={styles.text}>Capture Vitals</Text>
                        </View>
                        <Thumbnail style={styles.img} large source={require('../Public/img/clinic-logo-png.png')} />
                    </View>
                </View>
                <View style={styles.mainView}>
                    <Text style={styles.textd}>Device Successfully paired</Text>
                <View style={styles.circle} />
                <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#78797a',marginTop:20,marginBottom:10 }}>Device Name</Text>
                <Text style={styles.textcard}>Device ID</Text>
                <Text onPress={() => { this.props.navigation.navigate('StartTest') }} style={styles.textbottom}>Success</Text>
                </View>

            </Container>
        );
    }
}

const styles = StyleSheet.create({
    headerView: {
        justifyContent: 'space-between', flexDirection: 'row', width: wp('93%'), alignSelf: 'center', marginTop: 10
    },
    img: {
        height: 40, width: 40, borderRadius: 20, alignSelf: 'center', backgroundColor: 'white'
    },
    text: {
        marginLeft: 20, color: 'white', fontWeight: 'bold', fontSize: 18
    },
    scrollview: {
        justifyContent: 'space-between', flexDirection: 'row', width: wp('90%'), alignSelf: 'center', marginTop: 40, marginBottom: 20
    },
    circle: {
        height: 140, width: 140, borderRadius: 70, backgroundColor: '#a17167'

    },
   
    textcard: {
        color: '#95999c', fontSize: 12
    },
    mainView:{
        alignItems:'center',alignContent:'center',flex:1,marginTop:30
    },
    textd:{
            fontSize:18,fontWeight:'bold',marginBottom:20,color:'#78797a'
    },
    textbottom:{
        position:'absolute',bottom:70,fontWeight:'bold',color:'#95999c',fontSize:18
    }

});
