import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather'
import Icon1 from 'react-native-vector-icons/FontAwesome'
import IconUser from 'react-native-vector-icons/FontAwesome5'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast, Tab, Tabs
} from 'native-base';
const arr = [1, 2, 3]
export default class Drawer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action: false,
            listArray: [
                {
                    key: '0', route: 'MyAppointments', name: 'My Appointments'
                },
                {
                    key: '1', route: 'MyVitals', name: 'My Vitals'
                },
                {
                    key: '2', route: 'MyDoctorS', name: 'My Doctors'
                },
                {
                    key: '3', route: 'MyReports', name: 'My Documents and Reports'
                },
                {
                    key: '4', route: 'CliniQWallet', name: 'My CliniQ Wallet'
                },
                {
                    key: '5', route: 'MyInsurance', name: 'My Insurance'
                },
                {
                    key: '6', route: 'MyAccount', name: 'My Account'
                },
                {
                    key: '7', route: 'MedicalServices', name: 'Medical Services'
                },
                {
                    key: '8', route: '', name: 'My Emergency Contacts'
                },
                {
                    key: '9', route: '', name: 'Reminders'
                },
                {
                    key: '10', route: 'Referafriend', name: 'Refer a Friend'
                },
            ]
        };
    }
    func = (i) => {
        if(i.route!='')
        {
        this.props.closeModal()
        this.props.props1(i.route)
        }
        else{
            console.log("null")
        }
    }
    //    <Icon onPress={() => { this.props.closeModal()}} name='menu' size={30} />
    render() {
        console.log("props", this.props)
        return (
            <ScrollView>
                <Card style={styles.card}>
                    <View style={{ marginTop: 20, margin: 10 }}>
                        <Icon1 onPress={() => { this.props.closeModal() }} style={styles.iconcancle} color='#7a7979' name='times-circle' size={30} />
                        <View style={styles.view}>
                            <View style={styles.viewMap}>
                                <IconUser style={{ alignSelf: 'center', marginTop: 10 }} name='user-alt' size={40} color='grey' />
                            </View>
                            <View style={{ alignSelf: 'center', marginLeft: 10 }}>
                                <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'grey' }}>User Name</Text>
                                <Text style={{ fontSize: 13, color: '#67547d' }}>switch user</Text>
                            </View>
                        </View>
                        {this.state.listArray.map((i, j) => {
                            return (
                                <View style={{ marginLeft: 20 }}>
                                    {i.key == 8
                                        ?
                                        <View>
                                            <Text style={{ fontSize: 18, marginTop: 15, fontWeight: 'bold', color: '#a8a3a3' }}>{i.name}</Text>
                                            <View style={{ justifyContent: 'flex-start', flexDirection: 'row', marginTop: 15 }}>
                                                {arr.map((i, j) => {
                                                    return (
                                                        <View style={styles.viewMap1}>
                                                            <IconUser style={{ alignSelf: 'center', marginTop: 5 }} name='user-alt' size={30} color='#969393' />
                                                        </View>
                                                    )
                                                })}
                                            </View>
                                        </View>
                                        :
                                        <Text onPress={()=>{this.func(i)}} style={{ fontSize: 18, marginTop: 15, fontWeight: 'bold', color: '#545252' }}>{i.name}</Text>
                                    }
                                </View>
                            )
                        })}
                    </View>
                </Card>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    viewMap: {
        height: 70, width: 70, backgroundColor: 'white', borderRadius: 35, borderColor: 'grey', borderWidth: 1
    },
    card: {
        height: hp('100%'), width: wp('93%'), marginLeft: -2, marginTop: -2, marginBottom: -2
    },
    view: {
        justifyContent: 'flex-start', flexDirection: 'row'
    },
    iconcancle: {
        textAlign: 'right', alignSelf: 'center', position: 'absolute', top: 20, right: 5
    },
    viewMap1: {
        height: 50, width: 50, backgroundColor: 'white', borderRadius: 25, borderColor: '#969393', borderWidth: 1, marginRight: 15
    },
});
