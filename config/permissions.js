import { PermissionsAndroid } from 'react-native';


const requestLocationPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Location Permission',
          message:
            'App Needs access to your location, for showing fine results',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      console.log("grand",granted)
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can access location');
        return 'GRANTED';
      } else {
        console.log('Location permission denied');
        return 'DENIED';
      }
    } catch (err) {
        return false;
      console.warn('==',err);
    }
  }


  const requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: 'Camera Permission',
          message:
            'App Needs access to Camera',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // console.log('You can access camera');
        return 'GRANTED';
      } else {
        return 'DENIED';
        // console.log('Camera permission denied');
      }
    } catch (err) {
        return false;
      console.warn('==',err);
    }
  }


  const requestReadExtStoragePermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        {
          title: 'Read External Storage Permission',
          message:
            'App Needs access to external storage',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // console.log('You can access external storage');
        return 'GRANTED';
      } else {
        return 'DENIED';
        // console.log('External storage permission denied');
      }
    } catch (err) {
        return false;
      console.warn('==',err);
    }
  }

  const requestWriteCalendarPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_CALENDAR,
        {
          title: 'Calendar Permission',
          message:
            'App Needs access to calendar',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // console.log('You can access calendar');
        return 'GRANTED';
      } else {
        return 'DENIED';
        // console.log('Calendarpermission denied');
      }
    } catch (err) {
        return false;
      console.warn('==',err);
    }
  }

  const requestReadCalendarPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_CALENDAR,
        {
          title: 'Calendar Permission',
          message:
            'App Needs access to calendar',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // console.log('You can access calendar');
        return 'GRANTED';
      } else {
        return 'DENIED';
        // console.log('Calendarpermission denied');
      }
    } catch (err) {
        return false;
      console.warn('==',err);
    }
  }


  export { 
    requestLocationPermission,
    requestCameraPermission,
    requestReadExtStoragePermission,
    requestWriteCalendarPermission,
    requestReadCalendarPermission
  }
